<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Miasto extends Model
{
protected $primaryKey = 'id_miasta';
    protected $table = 'miasta';
     protected $fillable = [
        'nazwa', 'id_wojewodztwa'
    ];
}
