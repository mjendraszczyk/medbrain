<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rola extends Model
{
    protected $primaryKey = 'id_rola';
    protected $table = 'rola';
         protected $fillable = [
        'nazwa'
    ];
}
