<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poziom extends Model
{
    protected $primaryKey = 'id_poziom';
    protected $table = 'poziom';
     protected $fillable = [
        'nazwa'
    ];
}
