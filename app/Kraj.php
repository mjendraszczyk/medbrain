<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kraj extends Model
{
    protected $primaryKey = 'id_kraje';
    protected $table = 'kraje';
     protected $fillable = [
        'nazwa'
    ];
}
