<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edukacja extends Model
{
    protected $table = 'edukacja';
     protected $fillable = [
        'nazwa', 'edukacja_lata_od','edukacja_lata_do','id_user','id_tytul'
    ];
}
