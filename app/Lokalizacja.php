<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokalizacja extends Model
{
     protected $fillable = [
        'nazwa'
    ];
}
