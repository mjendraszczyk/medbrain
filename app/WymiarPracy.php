<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WymiarPracy extends Model
{
protected $primaryKey = 'id_wymiar_pracy';
    protected $table = 'wymiar_pracy';
    protected $fillable = [
        'nazwa'
    ];
}
