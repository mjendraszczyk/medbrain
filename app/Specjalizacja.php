<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specjalizacja extends Model
{
    protected $primaryKey = 'id_specjalizacje';
    protected $table = 'specjalizacje';
     protected $fillable = [
        'nazwa', 'kolor', 'symbol'
    ];
}
