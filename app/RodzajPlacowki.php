<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RodzajPlacowki extends Model
{

    protected $primaryKey = 'id_rodzaj_placowki';
    protected $table = 'rodzaj_placowki';
         protected $fillable = [
        'nazwa'
    ];
}
