<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ogloszenie extends Model
{
    protected $primaryKey = 'id_ogloszenia';
    protected $table = 'ogloszenia';
    protected $fillable = [
        'imie_nazwisko', 'email', 'id_user', 'id_specjalizacje', 'id_wymiar_pracy', 'id_doswiadczenie', 'wynagrodzenie', 'tresc', 'podmiot', 'id_podmiot'
    ];
}
