<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tytul extends Model
{
      protected $table = 'tytuly';
      protected $primaryKey = 'id_tytuly';
     protected $fillable = [
        'nazwa'
    ];
}
