<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $primaryKey = 'id_cms';
    protected $table = 'cms';
     protected $fillable = [
        'tytul', 'tresc', 'stan'
    ];
}
