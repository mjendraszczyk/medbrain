<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wojewodztwo extends Model
{
    protected $primaryKey = 'id_wojewodztwa';
    protected $table = 'wojewodztwa';
         protected $fillable = [
        'nazwa',
        'id_kraje'
    ];
}
