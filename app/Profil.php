<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $primaryKey = 'id_profil';
    protected $table = 'profil';
     protected $fillable = [
        'o_mnie','id_specjalizacje','id_user'
    ];
}
