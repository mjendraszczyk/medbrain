<?php

namespace App\Http\Controllers\backend;

use App\User;
use App\Platnosci;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Requests\backend\PlatnosciRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class PlatnosciController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $platnosci = User::paginate($this->limit);
        return view('backend.platnosci.index')->with('items',$platnosci);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.platnosci.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlatnosciRequest $request)
    {
        $platnosci = new Platnosci();
        $platnosci->nazwa = $request->input('nazwa');
        $platnosci->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $platnosci = Platnosci::where('id', $id)->first();

        return view('backend.platnosci.edit')->with('platnosci', $platnosci);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PlatnosciRequest $request, $id)
    {
        $platnosci = Platnosci::where('id', $id);
        $platnosci->update([
        'nazwa' => $request->input('nazwa'),
        ]);
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $platnosci = Platnosci::where('id_platnosci', $id);
        $platnosci->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new Platnosci()));
    }
}
