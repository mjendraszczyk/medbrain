<?php

namespace App\Http\Controllers\backend;

use App\Podmiot;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\backend\PodmiotRequest;


class PodmiotController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $podmioty = Podmiot::paginate($this->limit);
        return view('backend.podmiot.index')->with('items',$podmioty);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getSpecjalizacje = (new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        return view('backend.podmiot.create')->with('specjalizacje', $getSpecjalizacje)->with('wojewodztwa', $getWojewodztwa);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PodmiotRequest $request)
    {
        $podmiot = new Podmiot();
        $podmiot->nazwa = $request->input('nazwa');
        $podmiot->ulica = $request->input('ulica');
        $podmiot->id_miasta = $request->input('id_miasta');
        $podmiot->kod_pocztowy = $request->input('kod_pocztowy');
        $podmiot->lat = $request->input('lat');
        $podmiot->lon = $request->input('lon');
        $podmiot->nip = $request->input('nip');
        $podmiot->podmiot_telefon = $request->input('podmiot_telefon');
        $podmiot->podmiot_email = $request->input('podmiot_email');
        $podmiot->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $podmiot = Podmiot::where('id_podmiot', $id)->first();
        $getWojewodztwa = (new Wojewodztwo())->get();
        return view('backend.podmiot.edit')->with('podmiot', $podmiot)->with('wojewodztwa', $getWojewodztwa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PodmiotRequest $request, $id)
    {
        $podmiot = Podmiot::where('id_podmiot', $id);
        $podmiot->update([
        'nazwa' => $request->input('nazwa'),
        'ulica' => $request->input('ulica'),
        'id_miasta' => $request->input('id_miasta'),
        'kod_pocztowy' => $request->input('kod_pocztowy'),
        'lat' => $request->input('lat'),
        'lon' => $request->input('lon'),
        'nip' => $request->input('nip'),
        'podmiot_telefon' => $request->input('podmiot_telefon'),
        'podmiot_email' => $request->input('podmiot_email'),
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $podmiot = Podmiot::where('id_podmiot', $id);
        $podmiot->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
 
        return $this->filterCore($request, (new Podmiot()));
            // $columns = (new Podmiot())->getFillable();
            // $podmioty = Podmiot::where('id_podmiot', '>', 0);
            // foreach ($columns as $column) {
            //     // Session::put($column, '');
            //     if (($request->get($column) != '') || (Session::get($column) != '')) {
                    
            //         if (Session::get($column) == '') {
            //             Session::put($column, $request->get($column));
            //             $podmioty->where($column, 'LIKE', '%'.$request->get($column).'%');
            //         } else{
            //             $podmioty->where($column, 'LIKE', '%'.Session::get($column).'%');
            //         }
            //         // exit('f');
            //     }
            // }
            // if ($request->has('reset_filter')) {
            //     foreach ($columns as $column) {
            //         Session::put($column, '');
            //     }
            //     return redirect()->route('backend_podmiot_index');
            // } else {
            //     return view('backend.podmiot.index')->with('items', $podmioty->paginate($this->limit));
            // }
    }
}
