<?php

namespace App\Http\Controllers\backend;

use App\RodzajPlacowki;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\backend\RodzajPlacowkiRequest;


class RodzajPlacowkiController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rodzaj_placowki = RodzajPlacowki::paginate($this->limit);
        return view('backend.rodzaj_placowki.index')->with('items',$rodzaj_placowki);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.rodzaj_placowki.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RodzajPlacowkiRequest $request)
    {
        $rodzaj_placowki = new RodzajPlacowki();
        $rodzaj_placowki->nazwa = $request->input('nazwa');
        $rodzaj_placowki->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rodzaj_placowki = RodzajPlacowki::where('id_rodzaj_placowki', $id)->first();

        return view('backend.rodzaj_placowki.edit')->with('rodzaj_placowki', $rodzaj_placowki);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RodzajPlacowkiRequest $request, $id)
    {
        $rodzaj_placowki = RodzajPlacowki::where('id_rodzaj_placowki', $id);
        $rodzaj_placowki->update([
        'nazwa' => $request->input('nazwa'),
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rodzaj_placowki = RodzajPlacowki::where('id_rodzaj_placowki', $id);
        $rodzaj_placowki->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new RodzajPlacowki()));
    }
}
