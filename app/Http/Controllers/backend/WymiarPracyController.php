<?php

namespace App\Http\Controllers\backend;

use App\WymiarPracy;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Requests\backend\WymiarPracyRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class WymiarPracyController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wymiar_pracy = WymiarPracy::paginate($this->limit);
        return view('backend.wymiar_pracy.index')->with('items',$wymiar_pracy);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.wymiar_pracy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WymiarPracyRequest $request)
    {
        $wymiar_pracy = new WymiarPracy();
        $wymiar_pracy->nazwa = $request->input('nazwa');
        $wymiar_pracy->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wymiar_pracy = WymiarPracy::where('id_wymiar_pracy', $id)->first();

        return view('backend.wymiar_pracy.edit')->with('wymiar_pracy', $wymiar_pracy);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WymiarPracyRequest $request, $id)
    {
        $wymiar_pracy = WymiarPracy::where('id_wymiar_pracy', $id);
        $wymiar_pracy->update([
        'nazwa' => $request->input('nazwa'),
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wymiar_pracy = WymiarPracy::where('id_wymiar_pracy', $id);
        $wymiar_pracy->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new WymiarPracy()));
    }
}
