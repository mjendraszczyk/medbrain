<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackofficeController extends Controller
{
    public function index() { 
        return view('backend.backoffice.index');
    }
    public function redirect_403() { 
        return view('backend._main.403');
    }
}
