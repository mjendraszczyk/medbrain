<?php

namespace App\Http\Controllers\backend;

use App\Kraj;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Requests\backend\KrajeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class KrajeController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kraje = Kraj::paginate($this->limit);
        return view('backend.kraje.index')->with('items',$kraje);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kraje = Kraj::get();

        return view('backend.kraje.create')->with('kraje',$kraje);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KrajeRequest $request)
    {
        $kraje = new Kraj();
        $kraje->nazwa = $request->input('nazwa');
        $kraje->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kraje = Kraj::where('id_kraje', $id)->first();

        return view('backend.kraje.edit')->with('kraje', $kraje);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KrajeRequest $request, $id)
    {
        $kraje = Kraj::where('id_kraje', $id);
        $kraje->update([
        'nazwa' => $request->input('nazwa'),
        ]);
        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kraje = Kraj::where('id_kraje', $id);
        $kraje->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new Kraj()));
        // $object = new Kraj();
        // $primaryKey = ($object)->getKeyName();
        // $table = ($object)->getTable();
         
        //     $columns = ($object)->getFillable();
        //     // dd($request);
        
        //     $items = $object::where($primaryKey, '>', 0);
        //     foreach ($columns as $column) {
        //         if (($request->get($column) != '') || (Session::get($table.'_'.$column) != '')) {
                    
        //             if (Session::get($table.'_'.$column) == '') {
        //                 Session::put($table.'_'.$column, $request->get($column));
        //                 $items->where($column, 'LIKE', '%'.$request->get($column).'%');
        //             } else{
        //                 $items->where($column, 'LIKE', '%'.Session::get($table.'_'.$column).'%');
        //             }
        //         }
        //     }
        //     if ($request->has('reset_filter')) {
        //         foreach ($columns as $column) {
        //             Session::put($table.'_'.$column, '');
        //         }
        //         return redirect()->route('backend_'.$table.'_index');
        //     } else {
        //         return view('backend.'.$table.'.index')->with('items', $items->paginate($this->limit));
        //     }
    }
}
