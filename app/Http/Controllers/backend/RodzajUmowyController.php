<?php

namespace App\Http\Controllers\backend;

use App\RodzajUmowy;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\backend\RodzajUmowyRequest;


class RodzajUmowyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rodzaj_umowy = RodzajUmowy::paginate($this->limit);
        return view('backend.rodzaj_umowy.index')->with('items',$rodzaj_umowy);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.rodzaj_umowy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RodzajUmowyRequest $request)
    {
        $rodzaj_umowy = new RodzajUmowy();
        $rodzaj_umowy->nazwa = $request->input('nazwa');
        $rodzaj_umowy->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rodzaj_umowy = RodzajUmowy::where('id_rodzaj_umowy', $id)->first();

        return view('backend.rodzaj_umowy.edit')->with('rodzaj_umowy', $rodzaj_umowy);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RodzajUmowyRequest $request, $id)
    {
        $rodzaj_umowy = RodzajUmowy::where('id_rodzaj_umowy', $id);
        $rodzaj_umowy->update([
        'nazwa' => $request->input('nazwa'),
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rodzaj_umowy = RodzajUmowy::where('id_rodzaj_umowy', $id);
        $rodzaj_umowy->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new RodzajUmowy()));
    }
}
