<?php

namespace App\Http\Controllers\backend;

use App\Miasto;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Requests\backend\MiastaRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class MiastaController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $miasta = Miasto::paginate($this->limit);
        return view('backend.miasta.index')->with('items',$miasta);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wojewodztwo = Wojewodztwo::get();

        return view('backend.miasta.create')->with('wojewodztwa',$wojewodztwo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MiastaRequest $request)
    {
        $miasta = new Miasto();
        $miasta->nazwa = $request->input('nazwa');
        $miasta->id_wojewodztwa = $request->input('id_wojewodztwa');
        $miasta->save();
        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $miasta = Miasto::where('id_miasta', $id)->first();

        $wojewodztwo = Wojewodztwo::get();

        return view('backend.miasta.edit')->with('miasta', $miasta)->with('wojewodztwa', $wojewodztwo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MiastaRequest $request, $id)
    {
        $miasta = Miasto::where('id_miasta', $id);
        $miasta->update([
        'nazwa' => $request->input('nazwa'),
        'id_wojewodztwa' => $request->input('id_wojewodztwa'),
        ]);
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kraje = Miasto::where('id_miasta', $id);
        $kraje->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new Miasto()));
       
    }
}
