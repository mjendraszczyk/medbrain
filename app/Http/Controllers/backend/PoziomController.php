<?php

namespace App\Http\Controllers\backend;

use App\Poziom;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\PoziomRequest;
use Illuminate\Support\Facades\DB;


class PoziomController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $poziom = Poziom::paginate($this->limit);
        return view('backend.poziom.index')->with('items',$poziom);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.poziom.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PoziomRequest $request)
    {
        $poziom = new Poziom();
        $poziom->nazwa = $request->input('nazwa');
        $poziom->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $poziom = Poziom::where('id_poziom', $id)->first();

        return view('backend.poziom.edit')->with('poziom', $poziom);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PoziomRequest $request, $id)
    {
        $poziom = Poziom::where('id_poziom', $id);
        $poziom->update([
        'nazwa' => $request->input('nazwa'),
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $poziom = Poziom::where('id_poziom', $id);
        $poziom->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new Poziom()));
    }
}
