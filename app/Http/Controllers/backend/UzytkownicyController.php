<?php

namespace App\Http\Controllers\backend;

use App\User;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;
use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\UzytkownicyRequest;
use Illuminate\Support\Facades\DB;


class UzytkownicyController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uzytkownicy = User::paginate($this->limit);
        return view('backend.uzytkownicy.index')->with('items',$uzytkownicy);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.uzytkownicy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UzytkownicyRequest $request)
    {
        $uzytkownicy = new User();
        $uzytkownicy->name = $request->input('name');
        $uzytkownicy->email = $request->input('email');
        $uzytkownicy->stan = $request->input('stan');
        $uzytkownicy->telefon = $request->input('telefon');
        $uzytkownicy->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $uzytkownicy = User::where('id', $id)->first();

        return view('backend.uzytkownicy.edit')->with('uzytkownicy', $uzytkownicy);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UzytkownicyRequest $request, $id)
    {
        $uzytkownicy = User::where('id', $id);
        if(!empty($request->input('password'))) {

            

            if($request->input('password') == $request->input('password_repeat')) {
                $uzytkownicy->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'stan' => $request->input('stan'),
            'telefon' => $request->input('telefon'),
            'id_rola' => $request->input('id_rola'),
            ]);
            Session::flash('status', 'Zapisano pomyślnie.');
            } else {
              Session::flash('status_fail', 'Hasła do siebie nie pasują');
            }
        } else {
            $uzytkownicy->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'stan' => $request->input('stan'),
            'telefon' => $request->input('telefon'),
            'id_rola' => $request->input('id_rola'),
            ]);
             Session::flash('status', 'Zapisano pomyślnie.');
        }
           

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $uzytkownicy = User::where('id_uzytkownicy', $id);
        $uzytkownicy->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new User()));
    }
}
