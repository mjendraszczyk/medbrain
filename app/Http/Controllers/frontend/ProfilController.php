<?php

namespace App\Http\Controllers\frontend;

use App\Miasto;
use App\Poziom;
use App\User;
use App\Specjalizacja;
use App\Doswiadczenie;
use App\Wynagrodzenie;
use App\WymiarPracy;
use App\Wojewodztwo;
use App\RodzajPlacowki;
use App\RodzajUmowy;
use App\Ogloszenie;
use App\TypOgloszenia;
use App\Profil;
use Image;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\backend\OgloszeniaRequest;

use App\Edukacja;

use App\Podmiot;
use Auth;
use Session;


use App\Http\Requests\backend\UzytkownicyRequest;
use App\Http\Requests\frontend\ProfilLekarskiRequest;

use App\Http\Requests\backend\PodmiotRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfilController extends Controller
{
    public $getSpecjalizacje;
    public $getWojewodztwa;
    public $getOglosznia;
    public $getProfil;
    public $userID;
    public $user;

    public $doswiadczenie;
    public $edukacja;

    public function __construct()
    {
        $this->getSpecjalizacje = (new Specjalizacja())->get();
        $this->getWojewodztwa = (new Wojewodztwo())->get();
        $this->getOglosznia = (new Ogloszenie())->join('specjalizacje', 'ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('users', 'ogloszenia.id_user', '=', 'users.id')->leftJoin('podmiot', 'users.id_podmiot', '=', 'podmiot.id_podmiot')->get();
    }
    public function getAuth()
    {
        $this->user = Auth::user();
        return $this->user;
    }
    public function index()
    {
        return redirect()->route('login');
    }
    public function show($id)
    {
        $this->getProfil = (new User())->join('profil', 'profil.id_user', '=', 'users.id')->where('users.id', $id)->first();

        return view('frontend.profil.show')->with('specjalizacje', $this->getSpecjalizacje)->with('wojewodztwa', $this->getWojewodztwa)->with('ogloszenia', $this->getOglosznia)->with('profil', $this->getProfil);
    }
    public function dane()
    {
        $this->getProfil = (new User())->leftjoin('podmiot', 'users.id_podmiot', '=', 'podmiot.id_podmiot')->where('users.id', $this->getAuth()->id)->leftjoin('profil', 'profil.id_user', '=', 'users.id')->first();
 
        return view('frontend.profil.dane')->with('imie_nazwisko', $this->getAuth()->name)->with('specjalizacje', $this->getSpecjalizacje)->with('wojewodztwa', $this->getWojewodztwa)->with('ogloszenia', $this->getOglosznia)->with('profil', $this->getProfil);
    }

    public function profil_lekarski()
    {
        $this->getProfil = User::leftjoin('profil', 'users.id', '=', 'profil.id_user')->where('users.id', $this->getAuth()->id)->first();

        $this->edukacja = (new User())->leftjoin('edukacja', 'users.id', '=', 'edukacja.id_user')->where('users.id', $this->getAuth()->id)->first();
        $this->doswiadczenie =  User::leftjoin('doswiadczenie', 'users.id', '=', 'doswiadczenie.id_user')->where('users.id', $this->getAuth()->id)->first();
        
        return view('frontend.profil.profil_lekarski')->with('imie_nazwisko', $this->getAuth()->name)->with('specjalizacje', $this->getSpecjalizacje)->with('wojewodztwa', $this->getWojewodztwa)->with('ogloszenia', $this->getOglosznia)->with('profil', $this->getProfil)->with('edukacja', $this->edukacja)->with('doswiadczenie', $this->doswiadczenie);
    }

    public function platnosci()
    {
         $this->getProfil = User::leftjoin('profil', 'users.id', '=', 'profil.id_user')->where('users.id', $this->getAuth()->id)->first();

        return view('frontend.profil.platnosci')->with('imie_nazwisko', $this->getAuth()->name)->with('profil', $this->getProfil);
    }
    public function ogloszenia()
    {
        $id = Auth::user()->id;
        $this->getProfil = User::leftjoin('profil', 'users.id', '=', 'profil.id_user')->where('users.id', $this->getAuth()->id)->first();

        $getProfilOgloszenia = Ogloszenie::where('id_user', $id)->get();
        return view('frontend.profil.ogloszenia')->with('imie_nazwisko', $this->getAuth()->name)->with('ogloszenia', $getProfilOgloszenia)->with('profil', $this->getProfil);
    }
    public function editOgloszenie($id) {
        $this->getProfil = User::leftjoin('profil', 'users.id', '=', 'profil.id_user')->where('users.id', $this->getAuth()->id)->first();
        $ogloszenia = Ogloszenie::where('id_ogloszenia', $id)->where('id_user', Auth::user()->id)->first();
        $getWojewodztwa = (new Wojewodztwo())->get();
        return view('frontend.profil.edit')->with('ogloszenia', $ogloszenia)->with('wojewodztwa', $getWojewodztwa)->with('imie_nazwisko', $this->getAuth()->name)->with('profil', $this->getProfil);
    }
    public function updateOgloszenie(OgloszeniaRequest $request,$id) {
         $ogloszenia = Ogloszenie::where('id_ogloszenia', $id)->where('id_user', Auth::user()->id);
        
        $ogloszenia->update([
        'imie_nazwisko' => $request->input('imie_nazwisko'),
        'email' => Auth::user()->email,
        'id_user' => Auth::user()->id,
        'id_specjalizacje' => $request->input('id_specjalizacje'),
        'id_wymiar_pracy' => $request->input('id_wymiar_pracy'),
        'doswiadczenie_od' => $request->input('doswiadczenie_od'),
        'doswiadczenie_do' => $request->input('doswiadczenie_do'),
        'id_poziom' => $request->input('id_poziom'),
        'id_rodzaj_umowy' => $request->input('id_rodzaj_umowy'),
        'wynagrodzenie' => $request->input('wynagrodzenie'),
        'tresc' => $request->input('tresc'),
        'id_podmiot' => Auth::user()->id_podmiot,
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }
    public function updateDane(UzytkownicyRequest $request)
    {
        $id = Auth::user()->id;
        $upload = new Controller();

        $uzytkownicy = User::where('id', $id);
// dd($request);

        if ((Input::file('avatar_upload'))) {
            $upload->upload('avatar_upload', 'jpg,jpeg,png', '/img/avatar/', $request->get('avatar_upload'), $request);
            $uzytkownicy->update([
                'avatar_upload' => $upload->getFileName()
            ]);
        }

        if (!empty($request->input('password'))) {
            if ($request->input('password') == $request->input('password_repeat')) {
                $uzytkownicy->update([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                    'telefon' => $request->input('telefon'),
            ]);
                Session::flash('status', 'Zapisano pomyślnie.');
            } else {
                Session::flash('status_fail', 'Hasła do siebie nie pasują');
            }
        } else {
            $uzytkownicy->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'telefon' => $request->input('telefon'),
            ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        }
        return redirect()->back();
    }
    public function updateDaneFirma(PodmiotRequest $request)
    {
        //'nazwa', 'ulica', 'id_miasta', 'kod_pocztowy', 'lat', 'lon', 'nip','podmiot_telefon', 'podmiot_email'
        $id = Auth::user()->id_podmiot;
        $upload = new Controller();
        

        if ($id != null) {
            $podmiot = Podmiot::where('id_podmiot', $id);
            if ((Input::file('logo_upload'))) {
               
                $upload->upload('logo_upload', 'jpg,jpeg,png', '/img/logo/', $request->get('logo_upload'), $request);
                $podmiot->update([
                'logo_upload' => $upload->getFileName()
            ]);
            }
        }

        if ($id != null) {
            $podmiot = Podmiot::where('id_podmiot', $id);
            $podmiot->update([
                'nazwa' => $request->input('nazwa'),
                'ulica' => $request->input('ulica'),
                'id_miasta' => $request->input('id_miasta'),
                'kod_pocztowy' => $request->input('kod_pocztowy'),
                // 'lat' => $request->input('nazwa'),
                // 'lon' => $request->input('nazwa'),
                'nip' => $request->input('nip'),
                'podmiot_telefon' => $request->input('podmiot_telefon'),
                'podmiot_email' => $request->input('podmiot_email'),
            ]);
        }
        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }
    public function updateProfilLekarski(ProfilLekarskiRequest $request)
    {
        $profil = Profil::where('id_user', Auth::user()->id);
        $profil->update([
            'id_specjalizacje' => $request->input('id_specjalizacje'),
            'o_mnie' => $request->input('tresc'),
        ]);
        
        if (Profil::where('id_user', Auth::user()->id)->exists() == false) {
            $profil->insert([
            'id_specjalizacje' => $request->input('id_specjalizacje'),
            'o_mnie' => $request->input('tresc'),
            'id_user' => Auth::user()->id,
            ]);
        }


        Edukacja::where('id_user', Auth::user()->id)->delete();


        Edukacja::insert([
            'nazwa' => $request->input('edukacja_nazwa'),
            'edukacja_lata_od' => $request->input('edukacja_lata_od'),
            'edukacja_lata_do' => $request->input('edukacja_lata_do'),
            'id_tytul' => $request->input('id_tytul'),
            'id_user' => Auth::user()->id,
        ]);

        Doswiadczenie::where('id_user', Auth::user()->id)->delete();
        Doswiadczenie::insert([
            'nazwa' => $request->input('doswiadczenie_nazwa'),
            'doswiadczenie_lata_od' => $request->input('doswiadczenie_lata_od'),
            'doswiadczenie_lata_do' =>  $request->input('doswiadczenie_lata_do'),
            'id_user' => Auth::user()->id,
        ]);

        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }
}
