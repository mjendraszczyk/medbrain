<?php

namespace App\Http\Controllers\frontend;

use App\Miasto;
use App\Poziom;
use App\User;
use App\Specjalizacja;
use App\Doswiadczenie;
use App\Wynagrodzenie;
use App\WymiarPracy;
use App\Wojewodztwo;
use App\RodzajPlacowki;
use App\RodzajUmowy;
use App\Ogloszenie;
use App\TypOgloszenia;
use App\Podmiot;

use Illuminate\Http\Response;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\frontend\OgloszeniaKrok1Request;
use App\Http\Requests\frontend\OgloszeniaKrok2Request;
use App\Http\Requests\frontend\OgloszeniaKrok3Request;

use mrcnpdlk\Teryt\Client;
use mrcnpdlk\Teryt\NativeApi;

class OgloszeniaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.ogloszenia.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function krok1()
    {
        return view('frontend.ogloszenia.krok1');
    }
    public function krok1Request(OgloszeniaKrok1Request $request) { 

        $getRequestKrok1 = $request->input('specjalizacja');
         
        $request->session()->put('form_specjalizacja', $getRequestKrok1);
        return redirect()->route('oglosznia_dodaj_krok2');
    }
    public function krok2()
    {
        return view('frontend.ogloszenia.krok2');
    }

       public function krok2Request(OgloszeniaKrok2Request $request) { 

        $getRequestKrok2Wynagrodzenie = $request->input('wynagrodzenie');
        $getRequestKrok2DoswiadczenieOd = $request->input('doswiadczenie_od');
        $getRequestKrok2DoswiadczenieDo = $request->input('doswiadczenie_do');
        $getRequestKrok2Poziom = $request->input('poziom');
        $getRequestKrok2WymiarPracy = $request->input('wymiar_pracy');
        $getRequestKrok2Tresc = $request->input('tresc');
        $getRequestKrok2RodzajUmowy = $request->input('typ_umowy');
        $getRequestKrok2Szukam = $request->input('szukam');
         
        $request->session()->put('form_wynagrodzenie', $getRequestKrok2Wynagrodzenie);
        $request->session()->put('form_doswiadczenie_od', $getRequestKrok2DoswiadczenieOd);
        $request->session()->put('form_doswiadczenie_do', $getRequestKrok2DoswiadczenieDo);
        $request->session()->put('form_poziom', $getRequestKrok2Poziom);
        $request->session()->put('form_wymiar_pracy', $getRequestKrok2WymiarPracy);
        $request->session()->put('form_typ_umowy', $getRequestKrok2RodzajUmowy);
        $request->session()->put('form_tresc', $getRequestKrok2Tresc);
        
        $request->session()->put('form_szukam', $getRequestKrok2Szukam);

        return redirect()->route('oglosznia_dodaj_krok3');
    }
    public function krok3()
    {
        $getWojewodztwo = Wojewodztwo::get();
        $getRodzajPlacowki = RodzajPlacowki::get();
        return view('frontend.ogloszenia.krok3')->with('wojewodztwa', $getWojewodztwo)->with('rodzaje_placowki', $getRodzajPlacowki);
    }

       public function krok3Request(OgloszeniaKrok3Request $request) { 

        $getRequestKrok3ImieNazwisko = $request->input('imie_nazwisko');
        $getRequestKrok3Email = $request->input('adres_email');
        $getRequestKrok3Miasto = $request->input('miasto');
        $getRequestKrok3RodzajPlacowki = $request->input('rodzaj_placowki');
        $getRequestKrok3Ulica = $request->input('ulica');
        $getRequestKrok3Telefon = $request->input('telefon');
        $getRequestKrok3Podmiot = $request->input('podmiot');

        $getRequestKrok3Lat = $request->input('lat');
        $getRequestKrok3Lon = $request->input('lon');
        $getRequestKrok3Nip = $request->input('nip');
        
         
        $request->session()->put('form_imie_nazwisko', $getRequestKrok3ImieNazwisko);
        $request->session()->put('form_podmiot', $getRequestKrok3Podmiot);
        $request->session()->put('form_email', $getRequestKrok3Email);
        $request->session()->put('form_miasto', $getRequestKrok3Miasto);
        $request->session()->put('form_telefon', $getRequestKrok3Telefon);
        $request->session()->put('form_ulica', $getRequestKrok3Ulica);
        $request->session()->put('form_rodzaj_placowki', $getRequestKrok3RodzajPlacowki);
        $request->session()->put('form_lat', $getRequestKrok3Lat);
        $request->session()->put('form_lon', $getRequestKrok3Lon);
        $request->session()->put('form_nip', $getRequestKrok3Nip);
        


        return redirect()->route('oglosznia_dodaj_krok4');
    }

    public function krok4()
    {

        $doswiadczenie_od =  Session::get('form_doswiadczenie_od');
        $doswiadczenie_do =  Session::get('form_doswiadczenie_do');
        $miasto = Miasto::where('id_miasta', Session::get('form_miasto'))->first();
        $rodzaj_placowki = RodzajPlacowki::where('id_rodzaj_placowki', Session::get('form_rodzaj_placowki'))->first();
        $szukam = TypOgloszenia::where('id_typ_ogloszenia', Session::get('form_szukam'))->first();
        $poziom = Poziom::where('id_poziom', Session::get('form_poziom'))->first();
        $wynagrodzenie = Session::get('form_wynagrodzenie');//Wynagrodzenie::where('id', Session::get('form_wynagrodzenie'))->first();
        $specjalizacja = Specjalizacja::where('id_specjalizacje', Session::get('form_specjalizacja'))->first();
        $wymiar_pracy = WymiarPracy::where('id_wymiar_pracy', Session::get('form_wymiar_pracy'))->first();
        $typ_umowy = RodzajUmowy::where('id_rodzaj_umowy', Session::get('form_typ_umowy'))->first();

        return view('frontend.ogloszenia.krok4')
        ->with('doswiadczenie',$doswiadczenie->nazwa)
        ->with('imie_nazwisko',Session::get('form_imie_nazwisko'))
        ->with('miasto',$miasto->nazwa)
        ->with('rodzaj_placowki',$rodzaj_placowki->nazwa)
        ->with('email',Session::get('form_email'))
        ->with('poziom',$poziom->nazwa)
        ->with('wynagrodzenie',$wynagrodzenie)
        ->with('specjalizacja',$specjalizacja->nazwa)
        ->with('wymiar_pracy',$wymiar_pracy->nazwa)
        ->with('tresc',Session::get('form_tresc'))
        ->with('szukam',$szukam->nazwa)
        ->with('typ_umowy',$typ_umowy->nazwa);
    }
    public function krok4Request(Request $request) {


        $getWojewodztwo = Miasto::where('id_miasta', Session::get('form_miasto'))->first();
            $podmiot = new Podmiot(); 
            $podmiot->nazwa = Session::get('form_podmiot');
            $podmiot->ulica = Session::get('form_ulica');
            $podmiot->id_miasta = Session::get('form_miasto');
            $podmiot->id_wojewodztwo = $getWojewodztwo->id_wojewodztwo;
            $podmiot->id_kraj = '1';
            $podmiot->kod_pocztowy = '';
            $podmiot->lat = Session::get('form_lat');
            $podmiot->lon = Session::get('form_lon');
            $podmiot->nip = Session::get('form_nip');
            $podmiot->podmiot_telefon = Session::get('form_telefon');
            $podmiot->podmiot_email = Session::get('form_email');
            $podmiot->save();
            
             
            $ogloszenie = new Ogloszenie();
            $ogloszenie->email = Session::get('form_email');
            $ogloszenie->imie_nazwisko = Session::get('form_imie_nazwisko');
            $ogloszenie->id_user = 0;
            $ogloszenie->id_wymiar_pracy = Session::get('form_wymiar_pracy');
            $ogloszenie->id_specjalizacje = Session::get('form_specjalizacja');
            $ogloszenie->doswiadczenie_od = Session::get('form_doswiadczenie_od');
            $ogloszenie->doswiadczenie_do = Session::get('form_doswiadczenie_do');
            $ogloszenie->id_poziom = Session::get('form_poziom');
            $ogloszenie->wynagrodzenie = Session::get('form_wynagrodzenie');
            $ogloszenie->id_typ_ogloszenia = Session::get('form_szukam');
            $ogloszenie->id_rodzaj_umowy = Session::get('form_wymiar_pracy');
            $ogloszenie->tresc = Session::get('form_tresc');
            $ogloszenie->id_podmiot = $podmiot->id;
            $ogloszenie->save();
            // dd($podmiot->id);

        return redirect()->route('oglosznia_dodaj_krok5');
    }
    public function krok5()
    {
        return view('frontend.ogloszenia.krok5');
    }
    public function getOgloszenia($id_wojewodztwo, $id_specjalizacje) {//$id_wojewodztwo, $id_specjalizacje
        if (($id_specjalizacje == '0') && ($id_wojewodztwo == '0')){ 
            $ogloszenia = (new Ogloszenie())->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->get();
        } else {
            if ($id_specjalizacje == '0') {
                $ogloszenia = (new Ogloszenie())->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('miasta','miasta.id_miasta','=','podmiot.id_miasta')->where('miasta.id_wojewodztwa', $id_wojewodztwo)->get();
            } 
            else if ($id_wojewodztwo == '0') {
                $ogloszenia = (new Ogloszenie())->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->where('ogloszenia.id_specjalizacje', $id_specjalizacje)->get();
            } else {
                $ogloszenia = (new Ogloszenie())->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->where('ogloszenia.id_specjalizacje', $id_specjalizacje)->where('ogloszenia.id_specjalizacje', $id_specjalizacje)->get();
            }
        }
        return $ogloszenia;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function getOgloszeniaDependsOnWojewodztwo($id_wojewodztwa) {
        $getSpecjalizacje = (new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $getOgloszenia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->leftJoin('miasta','miasta.id_miasta','podmiot.id_miasta')->where('miasta.id_wojewodztwa', $id_wojewodztwa)->get();
        
        return view('frontend.ogloszenia.show')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getOgloszenia);
    }
    public function getOgloszeniaDependsOnSpecjalizacja($id_specjalizacje) { 
        $getSpecjalizacje = (new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $getOgloszenia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_specjalizacje', $id_specjalizacje)->get();
        
        return view('frontend.ogloszenia.show')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getOgloszenia);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getSpecjalizacje = (new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $getOglosznia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->get();
        $singleOgloszenie = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_ogloszenia', $id)->get();
        
        return view('frontend.ogloszenia.show')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getOglosznia)->with('ogloszenie',$singleOgloszenie);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getMiasta() { // tesotwa metoda

    $oClient = new Client();
    $oClient->setConfig('mjendraszczyk','Di9!neTp78',true);
    $oNativeApi = NativeApi::create($oClient);

    var_dump($oNativeApi->CzyZalogowany());
    var_dump($oNativeApi->PobierzSlownikCechULIC());
    //var_dump($oNativeApi->WyszukajMiejscowosc('skiernie',null));

    //var_dump($oNativeApi->PobierzListeWojewodztw());
    foreach($oNativeApi->PobierzListeWojewodztw() as $wojewodztwo) {
        //provinceId
 
        //var_dump($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId));

        foreach($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId) as $powiat) {
            //dd($powiat);
            //dd($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId));
            foreach($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId) as $gmina) {
            if ($gmina->typeName == 'miasto') { 
                //print_r($gmina);
                dd($gmina);
            }
        }
        }
    }
    //var_dump($oNativeApi->PobierzListePowiatow());

    }
}
