<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Wojewodztwo;
use App\Specjalizacja;
use App\Ogloszenie;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //
    public function index() {

        $getSpecjalizacje = (new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $getOglosznia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->get();
        return view('frontend.homepage.index')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getOglosznia);
    }
}
