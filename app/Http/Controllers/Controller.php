<?php

namespace App\Http\Controllers;

use App\Specjalizacja;
use App\WymiarPracy;
use App\Poziom;
use App\Wynagrodzenie;
use App\Doswiadczenie;
use App\Wojewodztwo;
use App\Miasto;
use App\Kraj;
use App\User;
use App\Podmiot;
use App\Ogloszenie;
use App\RodzajPlacowki;
use App\TypOgloszenia;
use App\RodzajUmowy;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Tytul;
use App\Rola;
use Image;
use App\Ustawienia;


use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $limit;
    public function __construct() {
        $this->limit = 5;
    }
    public static function getSpecjalizacje($id) { 
        if ($id == null) {
            $specjalizacje = (new Specjalizacja())->get();
        } else {
            $specjalizacje = (new Specjalizacja())->where('id_specjalizacje', $id)->first();
        }
        return $specjalizacje;
    }
        public static function getRole($id) { 
        if ($id == null) {
            $role = (new Rola())->get();
        } else {
            $role = (new Rola())->where('id_rola', $id)->first();
        }
        return $role;
    }
    public static function getSpecjalizacjaName($id) {
        $specjalizacjaName = (new Specjalizacja())->where('id_specjalizacje', $id)->first();
        return $specjalizacjaName;
    }
    public static function getWymiarPracy($id) { 
        if ($id == null) {
            $WymiarPracy = (new WymiarPracy())->get();
        } else {
            $WymiarPracy = WymiarPracy::where('id_wymiar_pracy', $id)->get();
        }
        return $WymiarPracy;
    }
    public static function getDoswiadczenie($id) { 
        if ($id == null) {
            $Doswiadczenie = (new Doswiadczenie())->get();
        } else {
            $Doswiadczenie = Doswiadczenie::where('id_doswiadczenie', $id)->get();
        }
        return $Doswiadczenie;
    }
    public static function getPoziom($id) { 
        if ($id == null) {
        $Poziom = (new Poziom())->get();
        } else {
            $Poziom = Poziom::where('id_poziom', $id)->first();
        }
        return $Poziom;
    }
    public static function getPodmiot($id) { 
        if ($id == null) {
        $Podmiot = (new Podmiot())->get();
        } else {
            $Podmiot = Podmiot::where('id_podmiot', $id)->first();
        }
        return $Podmiot;
    }
    public static function getWynagrodzenie($id) { 
        if($id == null) {
            $Wynagrodzenie = (new Wynagrodzenie())->get();
        } else {
            $Wynagrodzenie = (new Wynagrodzenie())->where('id_wynagrodzenie', $id)->get();
        }
        
        return $Wynagrodzenie;
    }
    public static function parseMoneyFormat($money) {
        return number_format($money, 2,'.', ' ').' zł';
    }
    public static function getMiastaByWojewodztwo($id) { 
        $getMiasta = Miasto::where('id_wojewodztwa', $id)->get();

        return $getMiasta;
    }
    public static function getRodzajPlacowki($id) {
        $getRodzajPlacowki = RodzajPlacowki::where('id_rodzaj_placowki', $id)->get(); 
        return $getRodzajPlacowki;
    }
    public static function getTypOgloszenia($id) {
        if ($id == null) {
            $getRodzajOgloszenia = (new TypOgloszenia())->get();
        } else {
            $getRodzajOgloszenia = TypOgloszenia::where('id_typ_ogloszenia', $id)->get();
        }
        return $getRodzajOgloszenia;
    }
    public static function getRodzajUmowy($id) {
        if ($id == null) {
            $getRodzajUmowy = (new RodzajUmowy())->get();
        } else {
            $getRodzajUmowy = RodzajUmowy::where('id_rodzaj_umowy', $id)->get();
        }
        return $getRodzajUmowy;
    }
    public static function getMiasto($id) {
        if($id == null) { 
            $getMiasto = Miasto::get();
            return $getMiasto->nazwa;
        } else {
            $getMiasto = Miasto::find($id);
            if ($getMiasto == null) {
                return "<Brak>";
            } else {
                $getMiasto = Miasto::where('id_miasta', $id)->first();
                return $getMiasto->nazwa;
            }
        }
    }
    public static function getUzytkownik($id) { 
        if ($id == null) {
            $getUzytkownik = User::get();
        } else{
            $getUzytkownik = User::where('id', $id)->first();
        }
        return $getUzytkownik;
    }
    public static function getTytuly($id) {
        if ($id == null) {
            $getTytuly = (new Tytul())->get();
        } else {
            $getTytuly = Tyul::where('id_tytul', $id)->get();
        }
        return $getTytuly;
    }
    public static function getKraj($id) {
        if($id == null) {
            $getKraj = Kraj::get();
        } else {
            $getKraj = Kraj::where('id_kraje', $id)->first();
        }
        return $getKraj;
    }
    public static function getWojewodztwo($id) {
        if($id == null) {
            $getWojewodztwo = Wojewodztwo::get();
        } else {
            $getWojewodztwo = Wojewodztwo::where('id_wojewodztwa', $id)->first();
        }
        return $getWojewodztwo;
    }
    public static function getKrajByIdMiasto($id) {
        $getMiasto = Miasto::find($id);

        if($getMiasto == null) {
            return "Brak";
        } else {
            $getMiasto->first();
            $getWojewodztwo = Wojewodztwo::findOrFail($getMiasto->id_wojewodztwa);
            if ($getWojewodztwo == null) {
                return  'Brak';
            } else {
                $getKraj = Kraj::find($getWojewodztwo->id_kraje);
 
                if ($getKraj == null) {
                    return  'Brak';
                } else {
                    $getKraj->first();
                    return $getKraj->nazwa;
                }
            }
        }
    }

    public static function getUstawienia() {
        $ustawienia = Ustawienia::pluck('wartosc', 'instancja');

        return $ustawienia;
    }

    public function filterCore($request, $object) {
        $primaryKey = ($object)->getKeyName();
        $table = ($object)->getTable();
         
            $columns = ($object)->getFillable();
            // dd($request);
        
            $items = $object::where($primaryKey, '>', 0);
            foreach ($columns as $column) {
                if (($request->get($column) != '') || (Session::get($table.'_'.$column) != '')) {
                    // echo Session::get($table.'_'.$column);
                    // echo $table.'_'.$column;
                    // dd($request);

                    // exit();
                    if (Session::get($table.'_'.$column) == '') {
                        // echo "Test";
                        // exit();
                        Session::put($table.'_'.$column, $request->get($column));
                        $items->where($column, 'LIKE', '%'.$request->get($column).'%');
                    } else{
                        if($request->get($column) != '') {
                            Session::put($table.'_'.$column, $request->get($column));
                        }
                        $items->where($column, 'LIKE', '%'.Session::get($table.'_'.$column).'%');
                    }
                }
            }
            if ($request->has('reset_filter')) {
                foreach ($columns as $column) {
                    Session::put($table.'_'.$column, '');
                }
                return redirect()->route('backend_'.$table.'_index');
            } else {
                return view('backend.'.$table.'.index')->with('items', $items->paginate($this->limit));
            }
    }

     public function upload($input, $extensions, $path, $tytul, $request)
    {
        $this->validate($request, [
            $input => 'image|mimes:'.$extensions.'|max:2048',
        ]);

        $image = $request->file($input);
        $this->FileName = md5(str_slug($tytul.time())).'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path($path);

        $img = Image::make($image->path());
        $img->resize(200, 200, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$this->FileName);

        // $imagePath = $destinationPath.'/'.$this->FileName;
        // $image->move($destinationPath, $this->FileName);
    }

    public function getFileName()
    {
        return $this->FileName;
    }
    public static function getOgloszeniaDependsPodmiot($id_podmiot)  {
         $getOgloszeniaDependsPodmiot = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_podmiot',$id_podmiot)->get();

         return $getOgloszeniaDependsPodmiot;
    }
}
