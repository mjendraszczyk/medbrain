<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class OgloszeniaKrok2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wynagrodzenie' => 'required',
            'doswiadczenie_od' => 'required',
            'doswiadczenie_do' => 'required',
            'wymiar_pracy' => 'required',
            'poziom' => 'required',
            'tresc' => 'required',
            'typ_umowy' => 'required',
            'szukam' => 'required',
        ];
    }
}
