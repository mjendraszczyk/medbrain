<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class OgloszeniaKrok3Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'imie_nazwisko' => 'required',
            'adres_email' => 'required',
            'miasto' => 'required',
            'rodzaj_placowki' => 'required',
            'ulica' => 'required',
            'telefon' => 'required',
            'podmiot' => 'required',
            'lat' => 'required',
            'lon' => 'required',
            'nip' => 'required',

        ];
    }
}
