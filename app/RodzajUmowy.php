<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RodzajUmowy extends Model
{
    protected $primaryKey = 'id_rodzaj_umowy';
    protected $table = 'rodzaj_umowy';
         protected $fillable = [
        'nazwa'
    ];
}
