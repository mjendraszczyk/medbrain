<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wynagrodzenie extends Model
{
    protected $primaryKey = 'id_wynagrodzenie';
    protected $table = 'wynagrodzenie';
     protected $fillable = [
        'nazwa'
    ];
}
