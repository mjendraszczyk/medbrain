<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use mrcnpdlk\Teryt\Client;
use mrcnpdlk\Teryt\NativeApi;


Auth::routes();

Route::get('/', 'frontend\HomeController@index')->name('home');

Route::get('/ogloszenia', 'frontend\OgloszeniaController@index')->name('ogloszenia_index');
Route::get('/ogloszenia/specjalizacja/{id_specjalizacje}', 'frontend\OgloszeniaController@getOgloszeniaDependsOnSpecjalizacja')->name('ogloszenia_specjalizacja_index');
Route::get('/ogloszenia/wojewodztwo/{id_wojewodztwa}', 'frontend\OgloszeniaController@getOgloszeniaDependsOnWojewodztwo')->name('ogloszenia_wojewodztwa_index');

Route::get('/lekarze/specjalizacja/{id_specjalizacje}', 'frontend\OgloszeniaController@getLekarzeDependsOnSpecjalizacja')->name('lekarze_specjalizacja_index');
Route::get('/lekarze/wojewodztwo/{id_wojewodztwa}', 'frontend\OgloszeniaController@getLekarzeDependsOnWojewodztwo')->name('lekarze_wojewodztwa_index');

Route::get('/ogloszenie/dodaj/krok1', 'frontend\OgloszeniaController@krok1')->name('oglosznia_dodaj_krok1');
Route::post('/ogloszenie/dodaj/krok1', 'frontend\OgloszeniaController@krok1Request')->name('krok1Request');

Route::get('/ogloszenie/dodaj/krok2', 'frontend\OgloszeniaController@krok2')->name('oglosznia_dodaj_krok2');
Route::post('/ogloszenie/dodaj/krok2', 'frontend\OgloszeniaController@krok2Request')->name('krok2Request');

Route::get('/ogloszenie/dodaj/krok3', 'frontend\OgloszeniaController@krok3')->name('oglosznia_dodaj_krok3');
Route::post('/ogloszenie/dodaj/krok3', 'frontend\OgloszeniaController@krok3Request')->name('krok3Request');

Route::get('/ogloszenie/dodaj/krok4', 'frontend\OgloszeniaController@krok4')->name('oglosznia_dodaj_krok4');
Route::post('/ogloszenie/dodaj/krok4', 'frontend\OgloszeniaController@krok4Request')->name('krok4Request');

Route::get('/ogloszenie/dodaj/krok5', 'frontend\OgloszeniaController@krok5')->name('oglosznia_dodaj_krok5');

Route::get('/ogloszenie/{id}', 'frontend\OgloszeniaController@show')->name('ogloszenia_zobacz');

Route::get('/faq', 'frontend\CmsController@faq')->name('faq');
Route::get('/kontakt', 'frontend\CmsController@kontakt')->name('kontakt');
Route::get('/o-nas', 'frontend\CmsController@onas')->name('onas');
Route::get('/konto', 'frontend\ProfilController@index')->name('konto');


Route::get('/regulamin', 'frontend\CmsController@regulamin')->name('regulamin');

Route::get('/pobierz-miasta', 'frontend\OgloszeniaController@getMiasta')->name('get_miasta');
Route::get('/api/ogloszenia/w/{id_wojewodztwa}/s/{id_specjalizacje}', 'frontend\OgloszeniaController@getOgloszenia')->name('get_ogloszenia');
Route::get('/api/ogloszenia/specjalizacja/{id_specjalizacje}', 'Controller@getSpecjalizacjaName')->name('get_specjalizacja_name');



Route::get('/profil/{id}', 'frontend\ProfilController@show')->name('profil_zobacz');

Route::get('/backoffice/dostep-zabroniony', 'backend\BackofficeController@redirect_403')->name('redirect_403');

Route::group(['middleware' => 'admin'], function(){
    Route::get('/backoffice', 'backend\BackofficeController@index')->name('backend_index');
    Route::get('/backoffice/uzytkownicy', 'backend\ProfilController@index')->name('backend_profil_index');
    Route::get('/backoffice/specjalizacje', 'backend\SpecjalizacjeController@index')->name('backend_specjalizacje_index');

    /**
    * Backend > Podmioty 
    */

    Route::get('/backoffice/podmioty', 'backend\PodmiotController@index')->name('backend_podmiot_index');
    Route::get('/backoffice/podmioty/create', 'backend\PodmiotController@create')->name('backend_podmiot_create');
    Route::get('/backoffice/podmioty/{id}/edit', 'backend\PodmiotController@edit')->name('backend_podmiot_edit');

    Route::get('/backoffice/podmioty/filter', 'backend\PodmiotController@filter')->name('backend_podmiot_filter_get');
    Route::post('/backoffice/podmioty/filter', 'backend\PodmiotController@filter')->name('backend_podmiot_filter');
    Route::delete('/backoffice/podmioty/{id}', 'backend\PodmiotController@destroy')->name('backend_podmiot_delete');
    Route::post('/backoffice/podmioty', 'backend\PodmiotController@store')->name('backend_podmiot_store');
    Route::put('/backoffice/podmioty/{id}', 'backend\PodmiotController@update')->name('backend_podmiot_update');
    
    /**
    * Backend > Kraje 
    */

    Route::get('/backoffice/kraje', 'backend\KrajeController@index')->name('backend_kraje_index');
    Route::get('/backoffice/kraje/create', 'backend\KrajeController@create')->name('backend_kraje_create');
    Route::get('/backoffice/kraje/{id}/edit', 'backend\KrajeController@edit')->name('backend_kraje_edit');

    Route::get('/backoffice/kraje/filter', 'backend\KrajeController@filter')->name('backend_kraje_filter_get');
    Route::post('/backoffice/kraje/filter', 'backend\KrajeController@filter')->name('backend_kraje_filter');
    Route::delete('/backoffice/kraje/{id}', 'backend\KrajeController@destroy')->name('backend_kraje_delete');
    Route::post('/backoffice/kraje', 'backend\KrajeController@store')->name('backend_kraje_store');
    Route::put('/backoffice/kraje/{id}', 'backend\KrajeController@update')->name('backend_kraje_update');

    /**
    * Backend > Miasta
    */

    Route::get('/backoffice/miasta', 'backend\MiastaController@index')->name('backend_miasta_index');
    Route::get('/backoffice/miasta/create', 'backend\MiastaController@create')->name('backend_miasta_create');
    Route::get('/backoffice/miasta/{id}/edit', 'backend\MiastaController@edit')->name('backend_miasta_edit');

    Route::get('/backoffice/miasta/filter', 'backend\MiastaController@filter')->name('backend_miasta_filter_get');
    Route::post('/backoffice/miasta/filter', 'backend\MiastaController@filter')->name('backend_miasta_filter');
    Route::delete('/backoffice/miasta/{id}', 'backend\MiastaController@destroy')->name('backend_miasta_delete');
    Route::post('/backoffice/miasta', 'backend\MiastaController@store')->name('backend_miasta_store');
    Route::put('/backoffice/miasta/{id}', 'backend\MiastaController@update')->name('backend_miasta_update');

    /**
     * Backend > Województwo
     */

    Route::get('/backoffice/wojewodztwa', 'backend\WojewodztwaController@index')->name('backend_wojewodztwa_index');
    Route::get('/backoffice/wojewodztwa/create', 'backend\WojewodztwaController@create')->name('backend_wojewodztwa_create');
    Route::get('/backoffice/wojewodztwa/{id}/edit', 'backend\WojewodztwaController@edit')->name('backend_wojewodztwa_edit');

    Route::get('/backoffice/wojewodztwa/filter', 'backend\WojewodztwaController@filter')->name('backend_wojewodztwa_filter_get');
    Route::post('/backoffice/wojewodztwa/filter', 'backend\WojewodztwaController@filter')->name('backend_wojewodztwa_filter');
    Route::delete('/backoffice/wojewodztwa/{id}', 'backend\WojewodztwaController@destroy')->name('backend_wojewodztwa_delete');
    Route::post('/backoffice/wojewodztwa', 'backend\WojewodztwaController@store')->name('backend_wojewodztwa_store');
    Route::put('/backoffice/wojewodztwa/{id}', 'backend\WojewodztwaController@update')->name('backend_wojewodztwa_update');

    /**
     *  Backend > Tytuly
     */

    Route::get('/backoffice/tytuly', 'backend\TytulyController@index')->name('backend_tytuly_index');
    Route::get('/backoffice/tytuly/create', 'backend\TytulyController@create')->name('backend_tytuly_create');
    Route::get('/backoffice/tytuly/{id}/edit', 'backend\TytulyController@edit')->name('backend_tytuly_edit');

    Route::get('/backoffice/tytuly/filter', 'backend\TytulyController@filter')->name('backend_tytuly_filter_get');
    Route::post('/backoffice/tytuly/filter', 'backend\TytulyController@filter')->name('backend_tytuly_filter');
    Route::delete('/backoffice/tytuly/{id}', 'backend\TytulyController@destroy')->name('backend_tytuly_delete');
    Route::post('/backoffice/tytuly', 'backend\TytulyController@store')->name('backend_tytuly_store');
    Route::put('/backoffice/tytuly/{id}', 'backend\TytulyController@update')->name('backend_tytuly_update');

    /**
     * Backend > Specjalizacje
     */
    
    Route::get('/backoffice/specjalizacje', 'backend\SpecjalizacjeController@index')->name('backend_specjalizacje_index');
    Route::get('/backoffice/specjalizacje/create', 'backend\SpecjalizacjeController@create')->name('backend_specjalizacje_create');
    Route::get('/backoffice/specjalizacje/{id}/edit', 'backend\SpecjalizacjeController@edit')->name('backend_specjalizacje_edit');

    Route::get('/backoffice/specjalizacje/filter', 'backend\SpecjalizacjeController@filter')->name('backend_specjalizacje_filter_get');
    Route::post('/backoffice/specjalizacje/filter', 'backend\SpecjalizacjeController@filter')->name('backend_specjalizacje_filter');
    Route::delete('/backoffice/specjalizacje/{id}', 'backend\SpecjalizacjeController@destroy')->name('backend_specjalizacje_delete');
    Route::post('/backoffice/specjalizacje', 'backend\SpecjalizacjeController@store')->name('backend_specjalizacje_store');
    Route::put('/backoffice/specjalizacje/{id}', 'backend\SpecjalizacjeController@update')->name('backend_specjalizacje_update');
    
    /**
     * Backend > Uzytkownicy
     */

    Route::get('/backoffice/uzytkownicy', 'backend\UzytkownicyController@index')->name('backend_uzytkownicy_index');
    Route::get('/backoffice/uzytkownicy/create', 'backend\UzytkownicyController@create')->name('backend_uzytkownicy_create');
    Route::get('/backoffice/uzytkownicy/{id}/edit', 'backend\UzytkownicyController@edit')->name('backend_uzytkownicy_edit');

    Route::get('/backoffice/uzytkownicy/filter', 'backend\UzytkownicyController@filter')->name('backend_uzytkownicy_filter_get');
    Route::post('/backoffice/uzytkownicy/filter', 'backend\UzytkownicyController@filter')->name('backend_uzytkownicy_filter');
    Route::delete('/backoffice/uzytkownicy/{id}', 'backend\UzytkownicyController@destroy')->name('backend_uzytkownicy_delete');
    Route::post('/backoffice/uzytkownicy', 'backend\UzytkownicyController@store')->name('backend_uzytkownicy_store');
    Route::put('/backoffice/uzytkownicy/{id}', 'backend\UzytkownicyController@update')->name('backend_uzytkownicy_update');

    /**
     * Backend > Placowki
     */

    Route::get('/backoffice/placowki', 'backend\RodzajPlacowkiController@index')->name('backend_rodzaj_placowki_index');
    Route::get('/backoffice/placowki/create', 'backend\RodzajPlacowkiController@create')->name('backend_rodzaj_placowki_create');
    Route::get('/backoffice/placowki/{id}/edit', 'backend\RodzajPlacowkiController@edit')->name('backend_rodzaj_placowki_edit');

    Route::get('/backoffice/placowki/filter', 'backend\RodzajPlacowkiController@filter')->name('backend_rodzaj_placowki_filter_get');
    Route::post('/backoffice/placowki/filter', 'backend\RodzajPlacowkiController@filter')->name('backend_rodzaj_placowki_filter');
    Route::delete('/backoffice/placowki/{id}', 'backend\RodzajPlacowkiController@destroy')->name('backend_rodzaj_placowki_delete');
    Route::post('/backoffice/placowki', 'backend\RodzajPlacowkiController@store')->name('backend_rodzaj_placowki_store');
    Route::put('/backoffice/placowki/{id}', 'backend\RodzajPlacowkiController@update')->name('backend_rodzaj_placowki_update');

    /**
    * Backend > Ogloszenia
    */

    Route::get('/backoffice/ogloszenia', 'backend\OgloszeniaController@index')->name('backend_ogloszenia_index');
    Route::get('/backoffice/ogloszenia/create', 'backend\OgloszeniaController@create')->name('backend_ogloszenia_create');
    Route::get('/backoffice/ogloszenia/{id}/edit', 'backend\OgloszeniaController@edit')->name('backend_ogloszenia_edit');

    Route::get('/backoffice/ogloszenia/filter', 'backend\OgloszeniaController@filter')->name('backend_ogloszenia_filter_get');
    Route::post('/backoffice/ogloszenia/filter', 'backend\OgloszeniaController@filter')->name('backend_ogloszenia_filter');
    Route::delete('/backoffice/ogloszenia/{id}', 'backend\OgloszeniaController@destroy')->name('backend_ogloszenia_delete');
    Route::post('/backoffice/ogloszenia', 'backend\OgloszeniaController@store')->name('backend_ogloszenia_store');
    Route::put('/backoffice/ogloszenia/{id}', 'backend\OgloszeniaController@update')->name('backend_ogloszenia_update');

    /**
    * Backend > Rodzaj umowy
    */

    Route::get('/backoffice/rodzaj-umowy', 'backend\RodzajUmowyController@index')->name('backend_rodzaj_umowy_index');
    Route::get('/backoffice/rodzaj-umowy/create', 'backend\RodzajUmowyController@create')->name('backend_rodzaj_umowy_create');
    Route::get('/backoffice/rodzaj-umowy/{id}/edit', 'backend\RodzajUmowyController@edit')->name('backend_rodzaj_umowy_edit');

    Route::get('/backoffice/rodzaj-umowy/filter', 'backend\RodzajUmowyController@filter')->name('backend_rodzaj_umowy_filter_get');
    Route::post('/backoffice/rodzaj-umowy/filter', 'backend\RodzajUmowyController@filter')->name('backend_rodzaj_umowy_filter');
    Route::delete('/backoffice/rodzaj-umowy/{id}', 'backend\RodzajUmowyController@destroy')->name('backend_rodzaj_umowy_delete');
    Route::post('/backoffice/rodzaj-umowy', 'backend\RodzajUmowyController@store')->name('backend_rodzaj_umowy_store');
    Route::put('/backoffice/rodzaj-umowy/{id}', 'backend\RodzajUmowyController@update')->name('backend_rodzaj_umowy_update');

    /**
    * Backend > Wymiar pracy
    */

    Route::get('/backoffice/wymiar-pracy', 'backend\WymiarPracyController@index')->name('backend_wymiar_pracy_index');
    Route::get('/backoffice/rodzaj-pracy/create', 'backend\WymiarPracyController@create')->name('backend_wymiar_pracy_create');
    Route::get('/backoffice/rodzaj-pracy/{id}/edit', 'backend\WymiarPracyController@edit')->name('backend_wymiar_pracy_edit');

    Route::get('/backoffice/rodzaj-pracy/filter', 'backend\WymiarPracyController@filter')->name('backend_wymiar_pracy_filter_get');
    Route::post('/backoffice/rodzaj-pracy/filter', 'backend\WymiarPracyController@filter')->name('backend_wymiar_pracy_filter');
    Route::delete('/backoffice/rodzaj-pracy/{id}', 'backend\WymiarPracyController@destroy')->name('backend_wymiar_pracy_delete');
    Route::post('/backoffice/rodzaj-pracy', 'backend\WymiarPracyController@store')->name('backend_wymiar_pracy_store');
    Route::put('/backoffice/rodzaj-pracy/{id}', 'backend\WymiarPracyController@update')->name('backend_wymiar_pracy_update');

    /**
    * Backend > CMS
    */

    Route::get('/backoffice/cms', 'backend\CmsController@index')->name('backend_cms_index');
    Route::get('/backoffice/cms/create', 'backend\CmsController@create')->name('backend_cms_create');
    Route::get('/backoffice/cms/{id}/edit', 'backend\CmsController@edit')->name('backend_cms_edit');

    Route::get('/backoffice/cms/filter', 'backend\CmsController@filter')->name('backend_cms_filter_get');
    Route::post('/backoffice/cms/filter', 'backend\CmsController@filter')->name('backend_cms_filter');
    Route::delete('/backoffice/cms/{id}', 'backend\CmsController@destroy')->name('backend_cms_delete');
    Route::post('/backoffice/cms', 'backend\CmsController@store')->name('backend_cms_store');
    Route::put('/backoffice/cms/{id}', 'backend\CmsController@update')->name('backend_cms_update');

    /**
     * Backoffice > Platnosci
     */

    Route::get('/backoffice/platnosci', 'backend\PlatnosciController@index')->name('backend_platnosci_index');
    Route::get('/backoffice/platnosci/create', 'backend\PlatnosciController@create')->name('backend_platnosci_create');
    Route::get('/backoffice/platnosci/{id}/edit', 'backend\PlatnosciController@edit')->name('backend_platnosci_edit');

    Route::get('/backoffice/platnosci/filter', 'backend\PlatnosciController@filter')->name('backend_platnosci_filter_get');
    Route::post('/backoffice/platnosci/filter', 'backend\PlatnosciController@filter')->name('backend_platnosci_filter');
    Route::delete('/backoffice/platnosci/{id}', 'backend\PlatnosciController@destroy')->name('backend_platnosci_delete');
    Route::post('/backoffice/platnosci', 'backend\PlatnosciController@store')->name('backend_platnosci_store');
    Route::put('/backoffice/platnosci/{id}', 'backend\PlatnosciController@update')->name('backend_platnosci_update');

    /**
     * Backoffice > Poziom
     */

    Route::get('/backoffice/poziom', 'backend\PoziomController@index')->name('backend_poziom_index');
    Route::get('/backoffice/poziom/create', 'backend\PoziomController@create')->name('backend_poziom_create');
    Route::get('/backoffice/poziom/{id}/edit', 'backend\PoziomController@edit')->name('backend_poziom_edit');

    Route::get('/backoffice/poziom/filter', 'backend\PoziomController@filter')->name('backend_poziom_filter_get');
    Route::post('/backoffice/poziom/filter', 'backend\PoziomController@filter')->name('backend_poziom_filter');
    Route::delete('/backoffice/poziom/{id}', 'backend\PoziomController@destroy')->name('backend_poziom_delete');
    Route::post('/backoffice/poziom', 'backend\PoziomController@store')->name('backend_poziom_store');
    Route::put('/backoffice/poziom/{id}', 'backend\PoziomController@update')->name('backend_poziom_update');

});

Route::group(['middleware' => 'auth'], function(){

    Route::get('/konto/dane', 'frontend\ProfilController@dane')->name('profil_dane');
    Route::put('/konto/dane', 'frontend\ProfilController@updateDane')->name('profil_dane_update');
    Route::put('/konto/dane/firma', 'frontend\ProfilController@updateDaneFirma')->name('profil_dane_firma_update');
    
    Route::get('/konto/profil-lekarski', 'frontend\ProfilController@profil_lekarski')->name('profil_profil-lekarski');
    Route::put('/konto/profil-lekarski', 'frontend\ProfilController@updateProfilLekarski')->name('profil_profil-lekarski_update');
    
    Route::get('/konto/ogloszenia', 'frontend\ProfilController@ogloszenia')->name('profil_ogloszenia');
    Route::get('/konto/oglosznenia/{id}/edit', 'frontend\ProfilController@editOgloszenie')->name('profil_ogloszenia_edit');
    Route::put('/konto/oglosznenia/{id}', 'frontend\ProfilController@updateOgloszenie')->name('profil_ogloszenia_update');

    Route::get('/konto/platnosci', 'frontend\ProfilController@platnosci')->name('profil_platnosci');

    /**
    * Backend > FAQ
    */

    Route::get('/backoffice/faq', 'backend\FaqController@index')->name('backend_faq_index');
    Route::get('/backoffice/faq/create', 'backend\FaqController@create')->name('backend_faq_create');
    Route::get('/backoffice/faq/{id}/edit', 'backend\FaqController@edit')->name('backend_faq_edit');

    Route::get('/backoffice/faq/filter', 'backend\FaqController@filter')->name('backend_faq_filter_get');
    Route::post('/backoffice/faq/filter', 'backend\FaqController@filter')->name('backend_faq_filter');
    Route::delete('/backoffice/faq/{id}', 'backend\FaqController@destroy')->name('backend_faq_delete');
    Route::post('/backoffice/faq', 'backend\FaqController@store')->name('backend_faq_store');
    Route::put('/backoffice/faq/{id}', 'backend\FaqController@update')->name('backend_faq_update');

    /**
    * Backend > Informacje o firmie
    */

    Route::get('/backoffice/witryna', 'backend\UstawieniaController@index')->name('backend_ustawienia_index');
    Route::get('/backoffice/witryna/create', 'backend\UstawieniaController@create')->name('backend_ustawienia_create');
    Route::get('/backoffice/witryna/{id}/edit', 'backend\UstawieniaController@edit')->name('backend_ustawienia_edit');

    Route::get('/backoffice/witryna/filter', 'backend\UstawieniaController@filter')->name('backend_ustawienia_filter_get');
    Route::post('/backoffice/witryna/filter', 'backend\UstawieniaController@filter')->name('backend_ustawienia_filter');
    Route::delete('/backoffice/witryna/{id}', 'backend\UstawieniaController@destroy')->name('backend_ustawienia_delete');
    Route::post('/backoffice/witryna', 'backend\UstawieniaController@store')->name('backend_ustawienia_store');
    Route::put('/backoffice/witryna/{id}', 'backend\UstawieniaController@update')->name('backend_ustawienia_update');
});

Route::get('/api/test', function() {
    $oClient = new Client();
    $oClient->setConfig('mjendraszczyk', 'Di9!neTp78', false);
    $oNativeApi = NativeApi::create($oClient);
    //$oNativeApi->CzyZalogowany();

    dd($oNativeApi->CzyZalogowany());
});
