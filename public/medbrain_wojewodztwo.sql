-- phpMyAdmin SQL Dump
-- version OVH
-- https://www.phpmyadmin.net/
--
-- Host: stylowetmjmarket.mysql.db
-- Czas generowania: 19 Gru 2019, 23:39
-- Wersja serwera: 5.6.43-log
-- Wersja PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `stylowetmjmarket`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `medbrain_wojewodztwo`
--

CREATE TABLE `medbrain_wojewodztwo` (
  `id_wojewodztwo` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_distinct` int(11) NOT NULL,
  `id_kraj` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `medbrain_wojewodztwo`
--

INSERT INTO `medbrain_wojewodztwo` (`id_wojewodztwo`, `nazwa`, `id_distinct`, `id_kraj`, `created_at`, `updated_at`) VALUES
(1, 'DOLNOŚLĄSKIE', 2, 1, '2019-12-02 10:17:32', '2019-12-02 10:17:32'),
(2, 'KUJAWSKO-POMORSKIE', 4, 1, '2019-12-02 10:17:35', '2019-12-02 10:17:35'),
(3, 'LUBELSKIE', 6, 1, '2019-12-02 10:17:38', '2019-12-02 10:17:38'),
(4, 'LUBUSKIE', 8, 1, '2019-12-02 10:17:41', '2019-12-02 10:17:41'),
(5, 'ŁÓDZKIE', 10, 1, '2019-12-02 10:17:42', '2019-12-02 10:17:42'),
(6, 'MAŁOPOLSKIE', 12, 1, '2019-12-02 10:17:44', '2019-12-02 10:17:44'),
(7, 'MAZOWIECKIE', 14, 1, '2019-12-02 10:17:46', '2019-12-02 10:17:46'),
(8, 'OPOLSKIE', 16, 1, '2019-12-02 10:17:49', '2019-12-02 10:17:49'),
(9, 'PODKARPACKIE', 18, 1, '2019-12-02 10:17:50', '2019-12-02 10:17:50'),
(10, 'PODLASKIE', 20, 1, '2019-12-02 10:17:53', '2019-12-02 10:17:53'),
(11, 'POMORSKIE', 22, 1, '2019-12-02 10:17:54', '2019-12-02 10:17:54'),
(12, 'ŚLĄSKIE', 24, 1, '2019-12-02 10:17:56', '2019-12-02 10:17:56'),
(13, 'ŚWIĘTOKRZYSKIE', 26, 1, '2019-12-02 10:17:59', '2019-12-02 10:17:59'),
(14, 'WARMIŃSKO-MAZURSKIE', 28, 1, '2019-12-02 10:18:00', '2019-12-02 10:18:00'),
(15, 'WIELKOPOLSKIE', 30, 1, '2019-12-02 10:18:02', '2019-12-02 10:18:02'),
(16, 'ZACHODNIOPOMORSKIE', 32, 1, '2019-12-02 10:18:05', '2019-12-02 10:18:05');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `medbrain_wojewodztwo`
--
ALTER TABLE `medbrain_wojewodztwo`
  ADD PRIMARY KEY (`id_wojewodztwo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
