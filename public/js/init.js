(function ($) {
  $(function () {

    $('.sidenav').sidenav();

  }); // end of document ready
})(jQuery); // end of jQuery name space


// new Vue({
//   el: '#app',
//   created() {
//     this.fetchData();
//   },
//   data: {
//     posts: []
//   },
//   methods: {
//     fetchData() {
//       axios.get('https://jsonplaceholder.typicode.com/posts').then(response => {
//         this.posts = response.data;
//       });
//     }
//   }
// });

/* <html lang="en">
  <head>
    <meta charset="utf-8">
    <title>//vuejsexamples.net</title>
    </head>
  <body>
    <div id="app">
      Posts:
      <li v-for="post of posts">
      <strong>{{post.title}}</strong>
      {{post.body}}
      </li>
    </div>
  </body>
</html> */