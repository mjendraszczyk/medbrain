<?php

use Illuminate\Database\Seeder;
use App\Specjalizacja;
use App\WymiarPracy;
use App\Wynagrodzenie;
use App\Poziom;
use App\Doswiadczenie;
use App\Wojewodztwo;
use App\User;
use App\Podmiot;
use App\Ogloszenie;
use App\Kraj;
use App\Miasto;
use App\RodzajPlacowki;
use App\RodzajUmowy;
use App\TypOgloszenia;
use App\Tytul;
use App\Rola;
use App\Profil;

use App\Ustawienia;
use App\Cms;
use App\Faq;

use mrcnpdlk\Teryt\Client;
use mrcnpdlk\Teryt\NativeApi;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        /**
         * Clear all data
         */
        Miasto::truncate();
        Specjalizacja::truncate();
        WymiarPracy::truncate();
        Doswiadczenie::truncate();
        Wojewodztwo::truncate();
        User::truncate();
        Poziom::truncate();
        Wynagrodzenie::truncate();
        Podmiot::truncate();
        Ogloszenie::truncate();
        Kraj::truncate();
        
        RodzajPlacowki::truncate();
        TypOgloszenia::truncate();
        RodzajUmowy::truncate();

        /**
         * Kraj
         */
        $kraj = new Kraj();
        $kraj->nazwa = 'Polska';
        $kraj->save();

        /**
         * Poziom
         */
        $poziomy = [
            'Asystent',
            'Pracownik',
            'Specjalista',
            'Dyrektor',
            'Kierownik',
            'Stazysta'
        ];

        foreach($poziomy as $poziom) { 
            $p = new Poziom();
            $p->nazwa = $poziom;
            $p->save();
        }

        /**
         * Wynagrodzenie
         */
         $wynagrodzenia = [
            '1000',
            '2000',
            '3000',
            '4000',
            '5000',
            '6000',
            '7000',
            '8000',
            '9000',
            '10000',
            '12000',
            '15000',
            '20000',
         ];

         foreach($wynagrodzenia as $wynagrodzenie) { 
            $w = new Wynagrodzenie();
            $w->nazwa = $wynagrodzenie;
            $w->save();
        }

 
 
        $kolory = [
        'bc208b',
        '2b5cb5',
        'f12b12',
        'fcbad3',
        'ffbe53',
        '4a5158',
        '20648e',
        '76c6dd',
        'f38081',
        '8361d0',
        '76dadd',
        '667785',
        'f12b12',
        '32b128',
        'a1e6a5'
        ];
        /* Specjalizacje */
        $specjalizacje_tab = [
            [
            'nazwa'=>'Anestezjologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'A',
            ],
            [
            'nazwa'=>'Seksuologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'S',
            ],
            [
            'nazwa'=>'Pulmunologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'PL',
            ],
            [
            'nazwa'=>'Onkologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'ON',
            ],
            [
            'nazwa'=>'Okulistyka',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'OK',
            ],
            [
            'nazwa'=>'Medycyna pracy',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'MP',
            ],
            [
            'nazwa'=>'Gastrologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'GA',
            ],
            [
            'nazwa'=>'Endokrynologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'EK',
            ],
            [
            'nazwa'=>'Diagnostyka',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'DL',
            ],
            [
            'nazwa'=>'Diabetologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'DIA',
            ],
            [
            'nazwa'=>'Choroby zakaźne',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'CHZ',
            ],
            [
            'nazwa'=>'Choroby wewnętrzne',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'CHW',
            ],
            [
            'nazwa'=>'Alergologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'AL',
            ],
            [
            'nazwa'=>'Ortopedia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'O',
            ],
            [
            'nazwa'=>'Kardiologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'K',
            ],
            [
            'nazwa'=>'Dietetyka',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'DT',
            ],
            [
            'nazwa'=>'Stomatologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'S',
            ],
            [
            'nazwa'=>'Neurologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'N',
            ],
            [
            'nazwa'=>'Urologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'U',
            ],
            [
            'nazwa'=>'Psychologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'PS',
            ],
            [
            'nazwa'=>'Psychiatria',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'PSC',
            ],
            [
            'nazwa'=>'Laryngologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'LR',
            ],
            [
            'nazwa'=>'Chirurgia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'C',
            ],
            [
            'nazwa'=>'Neurochirurgia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'NC',
            ],
            [
            'nazwa'=>'Radiologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'R',
            ],
            [
            'nazwa'=>'Pielęgniarstwo',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'PL',
            ],
            [
            'nazwa'=>'Pediatria',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'PD',
            ],
            [
            'nazwa'=>'Dermatologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'DR',
            ],
            [
            'nazwa'=>'Inne',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'I',
            ]
        ];
        foreach($specjalizacje_tab as $specjalizacja) {
            $specjalizacje = new Specjalizacja();
            $specjalizacje->nazwa = $specjalizacja['nazwa'];
            $specjalizacje->kolor = $specjalizacja['kolor'];
            $specjalizacje->symbol = $specjalizacja['symbol'];
            $specjalizacje->save();
        }

        /* Wymiar pracy */
        $wymiary_pracy = [
            'pełny etat',
            '1/2 etatu',
            '3/4 etatu',
            'inny',
        ];

        foreach($wymiary_pracy as $wymiar) {
            $wymiary = new WymiarPracy();
            $wymiary->nazwa = $wymiar;
            $wymiary->save();
        }

        /* Uzytkownicy */

        $uzytkownicy = [
            [
                'name' => 'Adam Kowalski',
                'email' => 'example0@example.com',
                'password' => '1',
                'stan' => '0',
            ],
            [
                'name' => 'Radosław Budzyński',
                'email' => 'example1@example.com',
                'password' => '1',
                'stan' => '0',
            ],
            [
                'name' => 'Tomasz Nowak',
                'email' => 'example2@example.com',
                'password' => '1',
                'stan' => '0',
            ],
            [
                'name' => 'Wojciech Wybicki',
                'email' => 'example3@example.com',
                'password' => '1',
                'stan' => '0',
            ],
            [
                'name' => 'Kazimierz Staropolski',
                'email' => 'example4@example.com',
                'password' => '1',
                'stan' => '0',
            ],
            [
                'name' => 'Pior Piotrkowski',
                'email' => 'example5@example.com',
                'password' => '1',
                'stan' => '0',
            ]

            ];

        foreach($uzytkownicy as $key => $uzytkownik) {
            $u = new User();
            $u->name = $uzytkownik['name'];
            $u->email = $uzytkownik['email'];
            $u->password = bcrypt($uzytkownik['password']);
            $u->stan = $uzytkownik['stan'];
            $u->id_podmiot = ($key+1);
            $u->id_rola = 1;
            $u->save();

            $profil = new Profil();
            $profil->id_user = $u->id;
            $profil->save();
        }


        /**
        *  Podmioty 
        */
            //'nazwa', 'ulica', 'id_miasta', 'id_wojewodztwo', 'id_kraj', 'kod_pocztowy', 'lat', 'lon', 'nip','telefon', 'email'
     $podmioty = [
         [
             'nazwa' => 'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Unii Lubelskiej 12',
             'id_miasta' => '938',
             'id_wojewodztwo' => '32',
             'id_kraj' => '1',
             'kod_pocztowy' => '71-001',
             'lat' => '53.4507381',
             'lon' => '14.50514',
             'nip' => '8510815059',
             'telefon' => '123 123 123',
             'email' => 'adres@domena.szczecin.pl'
         ],
         [
             'nazwa' => 'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Unii Lubelskiej 12',
             'id_miasta' => '91',
             'id_wojewodztwo' => '2',
             'id_kraj' => '1',
             'kod_pocztowy' => '53-001',
             'lat' => '51.1271646',
             'lon' => '16.9216529',
             'nip' => '8510815059',
             'telefon' =>'123 123 123',
             'email' => 'adres@domena.wroclaw.pl'
         ],
         [
             'nazwa' =>'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Przykladowa 1',
             'id_miasta' => '873',
             'id_wojewodztwo' => '30',
             'id_kraj' => '1',
             'kod_pocztowy' => '64-001',
             'lat' => '52.4006548',
             'lon' => '16.7612409',
             'nip' => '8510815059',
             'telefon' => '123 123 123',
             'email' => 'adres@domena.poznan.pl'
         ],
         [
             'nazwa' =>'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Spokojna 9',
             'id_miasta' => '593',
             'id_wojewodztwo' => '22',
             'id_kraj' => '1',
             'kod_pocztowy' => '43-001',
             'lat' => '54.3612059',
             'lon' => '18.5496024',
             'nip' => '8510815059',
             'telefon' => '123 123 123',
             'email' => 'adres@domena.gdansk.pl'
         ],
         [
             'nazwa' =>'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Szpitalna 20',
             'id_miasta' => '338',
             'id_wojewodztwo' => '12',
             'id_kraj' => '1',
             'kod_pocztowy' => '34-001',
             'lat' => '50.0468547',
             'lon' => '19.934662',
             'nip' => '8510815059',
             'telefon' => '123 123 123',
             'email' => 'adres@domena.krakow.pl'
         ],
         [
             'nazwa' =>'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Kliniczna 2',
             'id_miasta' =>  '940',
             'id_wojewodztwo' => '14',
             'id_kraj' => '1',
             'kod_pocztowy' => '00-001',
             'lat' => '52.2330649',
             'lon' => '20.9207691',
             'nip' => '8510815059',
             'telefon' => '123 123 123',
             'email' => 'adres@domena.warszawa.pl'
         ],
    ];

         foreach($podmioty as $podmiot) { 
            $p = new Podmiot();
            $p->nazwa = $podmiot['nazwa'];
            $p->ulica = $podmiot['ulica'];
            $p->id_miasta = $podmiot['id_miasta'];
            $p->kod_pocztowy = $podmiot['kod_pocztowy'];
            $p->lat = $podmiot['lat'];
            $p->lon = $podmiot['lon'];
            $p->nip = $podmiot['nip'];
            $p->podmiot_telefon = $podmiot['telefon'];
            $p->podmiot_email = $podmiot['email'];

            $p->save();

         }

         /**
          * Typ ogloszenia
          */
        $szukam = [
        'Pracownika',
        'Pracodawcy'
        ];
        foreach($szukam as $sz) {
            $typ_ogloszenia = new TypOgloszenia();
            $typ_ogloszenia->nazwa = $sz;
            $typ_ogloszenia->save();
        }
         /**
          * Rodzaj umowy
          */
          $rodzaje_umowy = [
            'Umowa o pracę',
            'Umowa zlecenie',
            'Umowa o dzieło',
            'B2B',
            'Inna'
          ];

          foreach($rodzaje_umowy as $rodzaj) {
            $um = new RodzajUmowy();
            $um->nazwa = $rodzaj;
            $um->save();
          }


         /**
          * Ogloszenia 
          */
          for($i=1;$i<=6;$i++) {
              //$getRandomSpecjalizacja = Specjalizacja::all()->random(1);
            $o = new Ogloszenie();
            $o->email = $podmioty[rand(0,count($podmioty)-1)]['email'];
            $o->imie_nazwisko = $uzytkownicy[rand(0,count($uzytkownicy)-1)]['name'];
            $o->id_user = $i;
            $o->id_wymiar_pracy = rand(1,4);
            $o->id_specjalizacje = rand(1,20);
            $o->doswiadczenie_od = rand(1,4);
            $o->doswiadczenie_do = rand(5,20);
            $o->id_podmiot = $i;
            $o->id_poziom = rand(1,6);
            $o->wynagrodzenie = rand(1000,15000);
            //$o->id_typ_ogloszenia = 1;//rand(1,2);
            $o->id_rodzaj_umowy = rand(1,5);//rand(1,2);
            $o->tresc = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quis metus sit amet dui euismod ornare ut at tortor. Integer id metus egestas, bibendum diam eu, convallis tortor. Donec vel urna nec lectus pulvinar semper at sed ante. Nunc gravida ac leo non rutrum. Phasellus in fringilla elit. Nullam massa diam, rutrum et purus ac, lobortis hendrerit ligula. Nulla interdum sed leo hendrerit imperdiet. Sed sit amet suscipit nisl. Suspendisse porta condimentum ante vitae venenatis. Aenean a augue erat. Integer eleifend varius risus eget aliquam. Donec tellus ante, vehicula et gravida nec, posuere tempus lacus. Nam fermentum tempus diam. <br/><br/>Fusce convallis nunc elementum, blandit neque rutrum, tristique tellus. Quisque justo massa, dignissim in ex id, mattis mollis libero. Mauris ut tortor nec ex laoreet ultricies. Vivamus molestie justo sit amet neque iaculis, vel rutrum orci convallis. Curabitur pretium ante lorem, non consequat velit sollicitudin vel. Morbi malesuada arcu tortor, et dictum ipsum imperdiet vel. Vivamus feugiat lorem eget porttitor tincidunt. Sed eu tellus vel eros posuere rhoncus eleifend vel enim. Sed erat metus, placerat non nisi ut, feugiat auctor elit. Suspendisse tempus risus est, ac ultrices neque lobortis in. Mauris ac iaculis elit, vitae viverra dui. Etiam faucibus auctor mauris nec varius. Curabitur vel sem vitae magna fermentum laoreet sit amet ac mauris. Aenean porta magna et lorem vestibulum, ut cursus velit rutrum. Vestibulum condimentum, arcu in sollicitudin sollicitudin, nulla est ultrices ante, quis lacinia justo nisl eget nisl.";
            $o->save();
          }
    
    /**
     * Rodzaje placowek
     */
            $tytuly = [
                'lic.',
                'inz.',
                'mgr',
                'mgr inz.',
                'dr',
                'dr inz.',
                'dr hab.',
                'dr hab. inz.',
                'prof',
            ];

            foreach($tytuly as $t) {
                    $tytul =  new Tytul();
                    $tytul->nazwa = $t;
                    $tytul->save();
            }



     $placowki = [
        'Klinika',
        'Szpital',
        'Przychodnia',
        'Laboratorium',
     ];

     foreach($placowki as $placowka) {
        $p = new RodzajPlacowki();
        $p->nazwa = $placowka;
        $p->save();
     }

     $role = [
         "Uzytkownik",
         "Firma",
         "Administrator"
     ];

     foreach ($role as $rola) {
         $r = new Rola();
         $r->nazwa = $rola;
         $r->save();
     }

    // exec("mysql -u root -p medbrain < ".asset('medbrain_wojewodztwo.sql'));
    //  exec("mysql -u root -p medbrain < ".asset('medbrain_miasto.sql'));

    $cmsy = [
        [
            'tytul' => 'O SERWISIE',
            'tresc' => '<div class="row white">
<div class="container">
 <div class="row">
  <div class="col s12 m12 l6">
  <div class="padding-box">
  <h3 class="title-section"><span class="green-text">Serwis ogłoszeń</span> Medbra.in</h3>
  Medbra.in to serwis ogłoszeń o pracę dla pracowników w branży medycznej w tym lekarzy, stomatologów, farmaceutów, fizjoterapeutów, pielęgniarzy, położnych, psychologów a także personelu wspomagającego, którego celem jest ułatwienie pracodawcom dotarcia do wysoko wykwalifikowanego personelu oraz pomoc pracownikom w znalezieniu spełniającej ich wymagania pracy.   
  </div>
  </div>
  <div class="col s12 m12 l6 onas1">
  </div>
</div>
</div>
</div>
<div class="row">
 <div class="container">
  <div class="col s12 m12 l6 onas2">
  
  </div>
  <div class="col s12 m12 l6">
  <div class="padding-box">
  <h3 class="title-section"><span class="green-text">Zalozenia</span> serwisu</h3>
  Medbra.in to serwis ogłoszeń o pracę dla pracowników w branży medycznej w tym lekarzy, stomatologów, farmaceutów, fizjoterapeutów, pielęgniarzy, położnych, psychologów a także personelu wspomagającego, którego celem jest ułatwienie pracodawcom dotarcia do wysoko wykwalifikowanego personelu oraz pomoc pracownikom w znalezieniu spełniającej ich wymagania pracy.   
  </div>
  </div>
</div>
</div>'
        ],
        [
            'tytul' => 'Regulamin',
            'tresc' => '<span class="green-text">Serwis ogłoszeń</span>
  Medbra.in to serwis ogłoszeń o pracę dla pracowników w branży medycznej w tym lekarzy, stomatologów, farmaceutów, fizjoterapeutów, pielęgniarzy, położnych, psychologów a także personelu wspomagającego, którego celem jest ułatwienie pracodawcom dotarcia do wysoko wykwalifikowanego personelu oraz pomoc pracownikom w znalezieniu spełniającej ich wymagania pracy.'
        ]
    ];

    foreach($cmsy as $cms) {
        $c = new Cms();
        $c->tytul = $cms['tytul'];
        $c->tresc = $cms['tresc'];
        $c->save();
    }
    
    /**
     * Faq
     */

     $faq = [
         [
             "nazwa" => "Czy musze sie rejestrowac?",
             "opis" => "Rejestracja jest niezbędna abyś mógł/mogła w pełni korzystać z funkcjonalności serwisu oraz abyśmy My mogli świadczyć dla Ciebie usługi na najwyższym poziomie. Tylko bowiem dla zarejestrowanych użytkowników możemy zaproponować indywidualne oferty i przesyłać powiadomienia o nowych ogłoszeniach, dzięki czemu będą one przesyłane Tobie tak szybko jak to możliwe. Ponadto rejestrując się będziesz mógł/mogła w szybki sposób publikować nowe ogłoszenia oraz otrzymywać informacje od kandydatów zainteresowanych ofertą. Sam proces rejestracji jest bardzo łatwy i bezpieczny."
         ],
         [
             "nazwa" => "Jak się zarejestrować?",
             "opis" => "W celu zarejestrowania nowego konta użytkownika musisz wypełnić odpowiedni formularz dla firm lub osób prywatnych. Formularz rejestracyjny dla firm znajdziesz w dziale 'szukam pracownika' a dla osób prywatnych w 'szukam pracy'. Jeśli poprawnie wypełniłeś zgłoszenie kliknij 'rejestruj' następnie otrzymasz od nas e-mail na podany w formularzu adres poczty elektronicznej z prośbą o potwierdzenie rejestracji konta. W celu potwierdzenia kliknij w link znajdujący się w treści e-maila, od tej chwili będziesz zarejestrowanym użytkownikiem serwisu medbra.in."
         ],
         [
             "nazwa" => "Jak zmienić hasło do mojego konta?",
             "opis" => "Jeśli zdecydujesz, że chcesz zmienić hasło do swojego konta w medbra.in wystarczy jak zalogujesz się i wybierzesz opcję 'zmiana hasła' dostępną w menu po lewej stronie. Następnie podaj nowe hasło i ponownie powtórz je w celu uniknięcia błędu, kliknij 'zapisz' od tego momentu nowe hasło jest aktywne."
         ],
         [
             "nazwa" => "Jak zmienić moje dane?",
             "opis" => "Wszelkie dane jakie wprowadziłeś w ramach swojego konta oraz profilu CV możesz w każdej chwili modyfikować. Po zalogowaniu do konta w medbra.in i masz możliwość wprowadzenia zmian w swoim CV jak i treści ogłoszenia."
         ],
         [
             "nazwa" => "Czy mogę otrzymywać oferty na maila?",
             "opis" => "Jasne, wystarczy że zaznaczysz odpowiednie pole w formularzu a My zadbamy abyś otrzymywał/a interesujące Cię oferty prosto na Twoją skrzynkę mailową."
         ],
         [
             "nazwa" => "Zapomniałem hasła, co mam zrobić?",
             "opis" => "Jeśli zapomniałeś hasła do swojego konta, wystarczy że skorzystasz z opcji 'przypomnienie hasła' dostępnego na stronie logowania. W formularzu podaj swój adres e-mail i kliknij 'wyślij hasło' nasz system wyśle na zarejestrowany adres hasło do Twojego konta."
         ],
         [
             "nazwa" => "Jak wyszukać interesujące mnie ogłoszenia?",
             "opis" => "Wystarczy skorzystać z Naszej wyszukiwarki, a w niej wybrać interesującą Ciebie kategorię i ewentualnie województwo, w którym poszukujesz pracy."
         ],
         [
             "nazwa" => "Jak zamieścić moje CV?",
             "opis" => "Jeśli chcesz zamieścić swoje CV w Naszej bazie wystarczy wypełnić formularz."
         ],
         [
             "nazwa" => "Czy mogę otrzymywać oferty na maila?",
             "opis" => "Jasne, wystarczy że zaznaczysz odpowiednie pole w formularzu a My zadbamy abyś otrzymywał/a interesujące Cię oferty prosto na Twoją skrzynkę mailową."
         ],
         [
             "nazwa" => "Jak się z Nami skontaktować?",
             "opis" => "Jeśli potrzebujesz dodatkowych informacji lub potrzebujesz dodatkowej pomocy np. przy publikacji ogłoszenia wystarczy, że napiszesz do Nas maila lub zadzwonisz a My postaramy się odpowiedzieć najszybciej jak tylko będzie to możliwe."
         ]
         ];

         foreach($faq as $f) {
             $fq = new Faq();
             $fq->nazwa = $f['nazwa'];
             $fq->opis = $f['opis'];
             $fq->save();
         }

         $ustawienia = [
            [
                'instancja' => 'email',
                'wartosc' => 'michal.jendraszczyk@gmail.com',
            ],
            [
                'instancja' => 'telefon',
                'wartosc' => '+48 123 123 123',
            ],
            [
                'instancja' => 'nazwa',
                'wartosc' => 'Medbrain',
            ],
            [
                'instancja' => 'opis',
                'wartosc' => 'Opis strony',
            ],
            [
                'instancja' => 'keywords',
                'wartosc' => 'medbrain, serwis, portal',
            ],
            [
                'instancja' => 'firma',
                'wartosc' => 'Medbrain sp. z o.o.',
            ],
            [
                'instancja' => 'adres',
                'wartosc' => 'ul. Cyfrowa 6, 71-441 Szczecin',
            ],
            [
                'instancja' => 'nip',
                'wartosc' => '8531515066',
            ],
         ];

foreach($ustawienia as $u) {
    $us = new Ustawienia(); 

    $us->instancja = $u['instancja'];
    $us->wartosc = $u['wartosc'];
    $us->save();
}

    $oClient = new Client();
    //$oClient->setConfig('mjendraszczyk', 'Di9!neTp78', true);
    $oClient->setConfig('mjendraszczyk', 'Di9!neTp78', true);
    $oNativeApi = NativeApi::create($oClient);
    $oNativeApi->CzyZalogowany();

    //var_dump($oNativeApi->PobierzListeWojewodztw());
    foreach ($oNativeApi->PobierzListeWojewodztw() as $wojewodztwo) {
        //provinceId

        /* Wojewodztwo */
 
            $w = new Wojewodztwo();
            $w->nazwa = $wojewodztwo->name;
            $w->id_distinct = $wojewodztwo->provinceId;
            $w->id_kraje = '1';
            $w->save();
 
        //var_dump($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId));

        foreach ($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId) as $powiat) {
            //dd($powiat);
            //dd($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId));
            foreach ($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId) as $gmina) {
                if (($gmina->typeName == 'miasto') || ($gmina->typeName == 'gmina miejska')){

                    $m = new Miasto();
                    $m->nazwa = $gmina->name;
                    $m->id_wojewodztwa = $w->id_wojewodztwa;//$gmina->provinceId;
                    $m->save();

                    //dd($gmina);
                }
            }
        }
    }

    $waw = new Miasto();
    $waw->nazwa = "Warszawa";
    $waw->id_wojewodztwa = "14";
    $waw->save();
 

    }
}
