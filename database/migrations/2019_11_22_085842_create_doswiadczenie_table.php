<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoswiadczenieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doswiadczenie', function (Blueprint $table) {
            $table->bigIncrements('id_doswiadczenie');
            $table->string('nazwa')->nullable();
            $table->date('doswiadczenie_lata_od')->nullable();
            $table->date('doswiadczenie_lata_do')->nullable();
            $table->integer('id_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doswiadczenie');
    }
}
