<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLokalizacjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lokalizacja', function (Blueprint $table) {
            $table->bigIncrements('id_lokalizacja');
            $table->string('ulica');
            $table->string('id_wojewodztwo');
            $table->string('id_miasta');
            $table->string('id_kraj');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lokalizacja');
    }
}
