<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdukacjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edukacja', function (Blueprint $table) {
            $table->bigIncrements('id_edukacja');
            $table->text('nazwa')->nullable();
            $table->date('edukacja_lata_od')->nullable();
            $table->date('edukacja_lata_do')->nullable();
            $table->integer('id_tytul')->nullable();
            $table->integer('id_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edukacja');
    }
}
