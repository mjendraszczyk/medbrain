@extends('layouts.medbrain')

@section('content')
<div class="row">
    <div class="">
        <div class="row center" style="display: flex;width:100%;">

            <div class="col s12 m12 l6 zarejestruj_box white-text">
                <div class="padding-box">
                    <h1 class="title-section bold rem3">Zarejestruj się</h1>
                    <p>Korzystaj z portalu {{config('app.name')}}</p>
                    <form method="POST" style="width: 50%;margin: auto;" action="{{ route('register') }}">
                        @csrf
                        <input id="email" type="email"
                            class="white-input white-text @error('email') is-invalid @enderror"  placeholder="email" name="email"
                            value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <ul class="alert alert-danger white-text">
                            <li>{{ $message }}</li>
                        </ul>
                        @enderror

                        <input id="password" type="password"
                            class="white-input white-text @error('password') is-invalid @enderror" placeholder="hasło" name="password"
                            required autocomplete="new-password">

                        @error('password')
                        <ul class="alert alert-danger white-text">
                            <li>{{ $message }}</li>
                        </ul>
                        @enderror

                        <input id="password-confirm" type="password" class="white-input white-text"
                            name="password_confirmation" required placeholder="powtórz hasło" autocomplete="new-password">

                            

                        <button type="submit" class="m-t50 btn waves-effect waves-light btn-large gray">Załóz swój
                            profil</button>
                    </form>
                </div>
            </div>

            <div class="col s12 m12 l6 zaloguj_box white-text">
                <div class="padding-box">
                    <h1 class="title-section bold rem3">Zaloguj się</h1>
                    <p>Jesli masz juz konto</p>
                    <form method="POST" action="{{ route('login') }}" style="width: 50%;" class="m-auto m-t50">
                        @csrf


                        <input id="email" type="email"
                            class="white-input white-text @error('email') is-invalid @enderror" name="email"
                            value="{{ old('email') }}" required autocomplete="email"  placeholder="email" autofocus>

                        @error('email')
                        <ul class="alert alert-danger white-text">
                            <li>{{ $message }}</li>
                        </ul>
                        @enderror

                        <input id="password" type="password"
                            class="white-input white-text @error('password') is-invalid @enderror" placeholder="hasło" name="password"
                            required autocomplete="current-password">

                        @error('password')
                        <ul class="alert alert-danger white-text">
                            <li>{{ $message }}</li>
                        </ul>
                        @enderror
                        <label style="display:flex;background: none !important;">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                {{ old('remember') ? 'checked' : '' }}>

                            <span class="white-text">
                                {{ __('Remember Me') }}
                            </span>
                        </label>

                        <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green">Zaloguj
                            się</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

<div class="card-body">
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                    value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <ul class="alert alert-danger white-text">
                            <li>{{ $message }}</li>
                        </ul>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" required autocomplete="current-password">

                @error('password')
                <ul class="alert alert-danger white-text">
                            <li>{{ $message }}</li>
                        </ul>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                        {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>

                @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif
            </div>
        </div>
    </form>
</div>
</div>
</div>
</div>
</div>--}}
@endsection