@extends('layouts.medbrain')

@section('content')
<div class="row section-faq">
    <h1 class="white-text bold">FAQ</h1>

</div>

<!-- 
     <div id="app">
<button v-on:click='isOpen = !isOpen'>Open/Close</button>
<span  v-show="isOpen">Toggle info</span>

<button v-on:click='isOpen = !isOpen'>Open/Close</button>
<span  v-show="isOpen">Toggle info</span>
</div>

new Vue({
el: '#app',
data: {
isOpen: false
},
methods:{
toggle: function(){
this.isOpen = !this.isOpen
}
}
});
  -->
<div class="row faq-section">

    <ol class="list list-striped">
        @foreach ($faq as $key => $f)
        <li>
            <div class="container">
                <span class="faq_count">{{($key+1)}}</span>
                {{$f->nazwa}}
                <p class="content_faq_box">
                    {{$f->opis}}
                </p>
            </div>
        </li>
        @endforeach
    </ol>
</div>

</div>

@endsection