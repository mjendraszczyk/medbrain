@extends('layouts.medbrain')

@section('content')
<div class="row center onas-section">
<h1 class="gray-text bold">REGULAMIN</h1>

</div>

<div class="row white">
<div class="container">
 <div class="row">

  <div class="col s12 m12 l12">
  <div class="padding-box">
  <h3 class="title-section"><span class="green-text">Serwis ogłoszeń</span> {{config('app.name')}}</h3>
  Medbra.in to serwis ogłoszeń o pracę dla pracowników w branży medycznej w tym lekarzy, stomatologów, farmaceutów, fizjoterapeutów, pielęgniarzy, położnych, psychologów a także personelu wspomagającego, którego celem jest ułatwienie pracodawcom dotarcia do wysoko wykwalifikowanego personelu oraz pomoc pracownikom w znalezieniu spełniającej ich wymagania pracy.   
  </div>
  </div>

</div>
</div>
</div>
 


</div>

@endsection