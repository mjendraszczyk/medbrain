@extends('layouts.medbrain')

@section('content')
<div class="row center onas-section">
<h1 class="gray-text bold">O SERWISIE</h1>

</div>

<div class="row white">
<div class="container">
 <div class="row">

  <div class="col s12 m12 l6">
  <div class="padding-box">
  <h3 class="title-section"><span class="green-text">Serwis ogłoszeń</span> {{config('app.name')}}</h3>
  Medbra.in to serwis ogłoszeń o pracę dla pracowników w branży medycznej w tym lekarzy, stomatologów, farmaceutów, fizjoterapeutów, pielęgniarzy, położnych, psychologów a także personelu wspomagającego, którego celem jest ułatwienie pracodawcom dotarcia do wysoko wykwalifikowanego personelu oraz pomoc pracownikom w znalezieniu spełniającej ich wymagania pracy.   
  </div>
  </div>
  
  <div class="col s12 m12 l6 onas1">
 
  </div>
</div>
</div>
</div>
 <div class="row">
 <div class="container">
  <div class="col s12 m12 l6 onas2">
  
  </div>
  <div class="col s12 m12 l6">
  <div class="padding-box">
  <h3 class="title-section"><span class="green-text">Zalozenia</span> serwisu</h3>
  Medbra.in to serwis ogłoszeń o pracę dla pracowników w branży medycznej w tym lekarzy, stomatologów, farmaceutów, fizjoterapeutów, pielęgniarzy, położnych, psychologów a także personelu wspomagającego, którego celem jest ułatwienie pracodawcom dotarcia do wysoko wykwalifikowanego personelu oraz pomoc pracownikom w znalezieniu spełniającej ich wymagania pracy.   
  </div>
  </div>
</div>
</div>


</div>

@endsection