<div class="avatar">
   
    @if($profil->avatar_upload != '')
    <img src="{{asset('img/avatar')}}/{{$profil->avatar_upload}}">
    @else
    <img src="{{asset('img/icons/avatar.png')}}">
    @endif
</div>
<ul class="menu">
    <h3 class="title_menu">Twój profil</h3>
    <li><a href="{{route('profil_dane')}}">Moje dane</a></li>
    <li><a href="{{route('profil_profil-lekarski')}}">Profil lekarski</a></li>
    <li><a href="{{route('profil_ogloszenia')}}">Moje ogłoszenia</a></li>
    <li><a href="{{route('profil_platnosci')}}">Moje płatnosci</a></li>
</ul>
<div class="logout">
    <form method="POST" action="{{route('logout')}}">
        @csrf
        <button type="submit">Wyloguj</button>
    </form>
</div>