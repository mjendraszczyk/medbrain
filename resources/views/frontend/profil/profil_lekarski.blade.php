@extends('layouts.medbrain')

@section('content')
<div class="container">
  <div class="profil row konto">
    <div class="col s12 m3 l3">
      <div class="white panel">
        @include('frontend.profil.menu')
      </div>
    </div>

    <div class="col s12 m9 l9">
      <div class="row white panel account_detail">
        @include('frontend.profil.profil_header')
        <h3 class="title_menu">Profil lekarski</h3>
        <form method="POST" id="app" novalidate action="{{route('profil_profil-lekarski_update')}}">
          @include('backend/_main/message')
          @csrf
          @method('PUT')
          <div class="input-field col s12">
            <div class="row">

              <div class="col s12 m12 l12">
                <label>Specjalizacja</label>
                <select name="id_specjalizacje" required>
                  <option value="" disabled selected>np. dermatologia</option>
                  @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
                  <option value="{{$specjalizacja->id_specjalizacje}}" @if($specjalizacja->id_specjalizacje ==
                    $profil->id_specjalizacje)
                    selected="selected"
                    @endif>{{$specjalizacja->nazwa}}</option>
                  @endforeach
                </select>
              </div>


              <div id="app3" class="col s12 m12 l12">
                <button type="button" @click="addRow" class="btn waves-effect waves-light btn-small green min-200"
                  style="display: block;">
                  <i class="material-icons">
                    add
                  </i>
                  Dodaj</button>
                <label>Edukacja</label>
                <div class="edukacja_block">
                  <input required placeholder="Edukacja" name="edukacja_nazwa" value="{{$edukacja->nazwa}}" type="text"
                    class="form-control-input validate">

                  <div class="row">
                    <div class="col s12 m2 l2">
                      <select required name="id_tytul">
                        <option value="" disabled selected>Tytuł</option>
                        @foreach (App\Http\Controllers\Controller::getTytuly(null) as
                        $tytul)
                        <option value="{{$tytul->id_tytul}}" @if($tytul->id_tytul == $edukacja->id_tytul)
                          selected="selected"
                          @endif>{{$tytul->nazwa}}</option>
                        @endforeach
                      </select>

                    </div>
                    <div class="col s12 m5 l5">
                      <input required placeholder="Lata od" value="{{$edukacja->lata_od}}" name="edukacja_lata_od"
                        type="date" min="0" class="form-control-input validate" />
                    </div>
                    <div class="col s12 m5 l5">
                      <input required placeholder="Lata do" value="{{$edukacja->lata_do}}" name="edukacja_lata_do"
                        type="date" min="0" class="form-control-input validate" />
                    </div>
                  </div>

                  <div class="row" v-for="(input, index) in inputs">
                    <input name="od" type="text" v-model="input.one"> - @{{input.one }} ^ @{{index}}
                    <input name="do" type="text" v-model="input.two"> - @{{ input.two }}
                    <button @click="deleteRow(index)">Delete</button>
                  </div>
                </div>
              </div>

              <div class="col s12 m12 l12">
                <button type="button" class="btn waves-effect waves-light btn-small green min-200"
                  style="display: block;">
                  <i class="material-icons">
                    add
                  </i>
                  Dodaj</button>
                <label>Doswiadczenie</label>
                <div class="doswiadczenie_block">
                  <input required placeholder="Doswiadczenie" name="doswiadczenie_nazwa"
                    value="{{$doswiadczenie->nazwa}}" type="text" class="form-control-input validate">
                  <div class="row">
                    <div class="col s12 m6 l6">
                      <input required placeholder="Staz od" value="{{$doswiadczenie->lata_od}}"
                        name="doswiadczenie_lata_od" type="date" class="form-control-input validate" />
                    </div>
                    <div class="col s12 m6 l6">
                      <input required placeholder="Staz do" value="{{$doswiadczenie->lata_do}}"
                        name="doswiadczenie_lata_do" type="date" class="form-control-input validate" />
                    </div>
                  </div>
                </div>

              </div>

              <div class="col s12 m12 l12">
                <label>O mnie</label>
                <textarea name="tresc" required placeholder="Wpisz tresc ogloszenia"
                  class="form-control-area materialize-textarea validate">{{$profil->o_mnie}}</textarea>
              </div>
            </div>

            <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green min-200">Zapisz</button>
          </div>


        </form>
      </div>
    </div>
  </div>
</div>
@endsection