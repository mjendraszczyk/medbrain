<div class="col s12 m8 l9 no-padding">
    <h3 class="big">{{$imie_nazwisko}}</h3>
</div>
<div class="col s12 m4 l3" style="display: inline-flex;">
    <a href="{{route('profil_zobacz',['id'=>$profil->id])}}" target="_blank" style="margin: 0 5px;"
        class="btn border-flex-btn">
        <i class="material-icons">
            people
        </i>
        Profil</a>

    <a href="{{route('ogloszenia_specjalizacja_index',['id'=>$profil->id_specjalizacje])}}" class="btn border-flex-btn">
        <i class="material-icons">
            work_outline
        </i>Praca</a>
</div>