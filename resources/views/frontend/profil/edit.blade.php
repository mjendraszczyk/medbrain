@extends('layouts.medbrain')

@section('content')
<div class="container">
    <div class="profil row konto">
        <div class="col s12 m3 l3">
            <div class="white panel">
                @include('frontend.profil.menu')
            </div>
        </div>

        <div class="col s12 m9 l9">
            <div class="row white panel account_detail">
                @include('frontend.profil.profil_header')
                <h3 class="title_menu">Twoje dane</h3>
                <form method="POST"
                    action="{{route('profil_ogloszenia_update', ['id' => $ogloszenia->id_ogloszenia])}}">
                    @method('PUT')
                    @csrf
                    <div class="col s12 m12 l12">
                        @include('backend/_main/message')

                        <div class="clearfix"></div>
                        <label>Specjalizacja</label>
                        <select name="id_specjalizacje">
                            <option value="" disabled selected>np. dermatologia</option>
                            @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
                            <option value="{{$specjalizacja->id_specjalizacje}}"
                                @if((Route::currentRouteName()=='profil_ogloszenia_edit' ) && ($ogloszenia->
                                id_specjalizacje ==
                                ($specjalizacja->id_specjalizacje)))
                                selected="selected"
                                @else
                                @endif>{{$specjalizacja->nazwa}}</option>
                            @endforeach
                        </select>
                        <div class="clearfix"></div>
                        <label>Wynagrodzenie</label>
                        <input placeholder="Wynagrodzenie" name="wynagrodzenie"
                            @if(Route::currentRouteName()=='profil_ogloszenia_edit' )
                            value="{{$ogloszenia->wynagrodzenie}}" @else value="" @endif type="number"
                            class="form-control-input validate">

                        <div class="row">
                            <div class="col s12 m6 l6">
                                <label>Doswiadczenie od:</label>

                                <input required placeholder="Doswiadczenie od" name="doswiadczenie_od"
                                    @if(Route::currentRouteName()=='profil_ogloszenia_edit' )
                                    value="{{$ogloszenia->doswiadczenie_od}}" @else value="" @endif type="text"
                                    class="form-control-input validate">
                            </div>

                            <div class="col s12 m6 l6">
                                <label>Doswiadczenie do:</label>

                                <input required placeholder="Doswiadczenie do" name="doswiadczenie_do"
                                    @if(Route::currentRouteName()=='profil_ogloszenia_edit' )
                                    value="{{$ogloszenia->doswiadczenie_do}}" @else value="" @endif type="text"
                                    class="form-control-input validate">
                            </div>

                        </div>

                        <div class="clearfix"></div>
                        <label>Poziom</label>
                        <select name="id_poziom">
                            <option value="" disabled selected>Poziom</option>
                            @foreach(App\Http\Controllers\Controller::getPoziom(null) as $poziom)
                            <option value="{{$poziom->id_poziom}}"
                                @if((Route::currentRouteName()=='profil_ogloszenia_edit' ) && ($poziom->
                                id_poziom ==
                                $ogloszenia->id_poziom))
                                selected="selected"
                                @else @endif>{{$poziom->nazwa}}</option>
                            @endforeach
                        </select>
                        <div class="clearfix"></div>
                        <label>Typ umowy</label>
                        <select name="id_rodzaj_umowy">
                            <option value="" disabled selected>Rodzaj umowy</option>
                            @foreach(App\Http\Controllers\Controller::getRodzajUmowy(null) as $rodzaj)
                            <option value="{{$rodzaj->id_rodzaj_umowy}}"
                                @if((Route::currentRouteName()=='profil_ogloszenia_edit' ) && ($rodzaj->id_rodzaj_umowy
                                ==
                                $ogloszenia->id_rodzaj_umowy))
                                selected="selected"
                                @else @endif>{{$rodzaj->nazwa}}</option>
                            @endforeach
                        </select>
                        <div class="clearfix"></div>
                        <label>Wymiar pracy</label>
                        <select name="id_wymiar_pracy">
                            <option value="" disabled selected>Wymiar pracy</option>
                            @foreach(App\Http\Controllers\Controller::getWymiarPracy(null) as $wymiar)
                            <option value="{{$wymiar->id_wymiar_pracy}}"
                                @if((Route::currentRouteName()=='profil_ogloszenia_edit' ) && ($wymiar->id_wymiar_pracy
                                ==
                                $ogloszenia->id_wymiar_pracy))
                                selected="selected"
                                @else @endif>{{$wymiar->nazwa}}</option>
                            @endforeach
                        </select>
                        <div class="clearfix"></div>
                        <label>Tresc</label>
                        @if(Route::currentRouteName()=='profil_ogloszenia_edit' )
                        <textarea name="tresc" required placeholder="Wpisz tresc ogloszenia"
                            class="form-control-area materialize-textarea validate">{{$ogloszenia->tresc}}</textarea>
                        @else
                        <textarea name="tresc" required placeholder="Wpisz tresc ogloszenia"
                            class="form-control-area materialize-textarea validate">{{old('tresc')}}</textarea>
                        @endif
                    </div>

                    <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green min-200">
                        Zapisz</button>
                </form>
            </div>

        </div>
    </div>
</div>
</div>
@endsection