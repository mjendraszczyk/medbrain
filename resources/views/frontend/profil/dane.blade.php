@extends('layouts.medbrain')

@section('content')

<div class="profil container">
  <div class="row konto">
    <div class="col s12 m3 l3">
      <div class="white panel">
        @include('frontend.profil.menu')
      </div>
    </div>

    <div class="col s12 m9 l9">

      {{--     
      @foreach($profil as $p)
      --}}
      <div class="row white panel account_detail">
        @include('frontend.profil.profil_header')

        <h3 class="title_menu" style="margin:10px 0;">Twoje dane</h3>

        <div class="uzytkownik_dane">
          @include('backend/_main/message')
          <div class="input-field col s6">
            <div class="row card padding-25 no-shadow">
              <h3 class="card-title">Uzytkownik</h3>
              <form method="POST" enctype="multipart/form-data" action="{{route('profil_dane_update')}}">
                @csrf
                @method('PUT')

                <div class="clearfix"></div>
                <label>Imię i nazwisko</label>
                <input required placeholder="Imię i nazwisko" name="name" value="{{$profil->name}}" type="text"
                  class="form-control-input validate">
                <div class="clearfix"></div>
                <label>E-mail</label>
                <input required placeholder="E-mail" name="email" value="{{$profil->email}}" type="text"
                  class="form-control-input validate">
                <div class="clearfix"></div>
                <label>Hasło</label>
                <input placeholder="Hasło" name="password" value="" type="password" class="form-control-input validate">

                <label>Powtórz Hasło</label>
                <input placeholder="Powtórz Hasło" name="password_repeat" value="" type="password"
                  class="form-control-input validate">

                <label>Telefon</label>
                <input placeholder="Telefon" value="{{$profil->telefon}}" name="telefon" type="text"
                  class="form-control-input validate">

                <label>Avatar</label>
                <div class="file-field input-field">
                  <div class="btn">
                    <span>Plik</span>
                    <input type="file" name="avatar_upload">
                  </div>
                  <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                  </div>
                </div>

                <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green min-200">Zapisz</button>
              </form>
            </div>
          </div>

          <div class="input-field col s6">
            <div class="row card padding-25 no-shadow no-border">
              @if($profil->status == 0)
              <form method="POST" enctype="multipart/form-data" action="{{route('profil_dane_firma_update')}}">
                @csrf
                @method('PUT')
                <h3 class="card-title">Firma</h3>
                <div class="col s12 m12 l12">
                  <div class="clearfix"></div>
                  <label>Nazwa firmy</label>
                  <input required placeholder="Nazwa firmy" name="nazwa" value="{{$profil->nazwa}}" type="text"
                    class="form-control-input validate">
                </div>
                <div class="col s12 m12 l12">
                  <label>NIP</label>
                  <input required placeholder="NIP" name="nip" value="{{$profil->nip}}" type="text"
                    class="form-control-input validate">
                </div>

                <div class="col s12 m12 l12">

                  <label>E-mail słuzbowy</label>
                  <input required placeholder="E-mail" name="podmiot_email" value="{{$profil->podmiot_email}}"
                    type="email" class="form-control-input validate">

                  <label>Telefon słuzbowy</label>
                  <input required placeholder="Telefon" value="{{$profil->podmiot_telefon}}" name="podmiot_telefon"
                    type="text" class="form-control-input validate">
                </div>
                <div class="col s12 m12 l12">
                  <label>Ulica</label>
                  <input required placeholder="Ulica" name="ulica" value="{{$profil->ulica}}" type="text"
                    class="form-control-input validate">

                  <label>Kod pocztowy</label>
                  <input required placeholder="Kod pocztowy" name="kod_pocztowy" value="{{$profil->kod_pocztowy}}"
                    type="text" class="form-control-input validate">

                  <label>Miasto</label>
                  <select required name="id_miasta">
                    <option value="" disabled selected>Województwo</option>
                    @foreach ($wojewodztwa as $wojewodztwo)
                    <optgroup label="{{$wojewodztwo->nazwa}}">
                      @foreach (App\Http\Controllers\Controller::getMiastaByWojewodztwo($wojewodztwo->id_wojewodztwa) as
                      $miasto)
                      <option value="{{$miasto->id_miasta}}" @if($miasto->id_miasta == $profil->id_miasta)
                        selected="selected"
                        @endif>{{$miasto->nazwa}}</option>
                      @endforeach
                    </optgroup>
                    @endforeach
                  </select>

                  <div class="clearfix"></div>
                  <label>Logo</label>
                  <div class="file-field input-field">
                    <div class="btn">
                      <span>Plik</span>
                      <input type="file" name="logo_upload">
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path validate" type="text">
                    </div>
                  </div>

                  <input required placeholder="Lat" name="lat" id="lat" value="{{$profil->lat}}" type="hidden"
                    class="form-control-input validate">
                  <input required placeholder="Lon" name="lon" id="lon" value="{{$profil->lon}}" type="hidden"
                    class="form-control-input validate">

                  <button type="submit"
                    class="m-t50 btn waves-effect waves-light btn-large green min-200">Zapisz</button>
              </form>
              @else
              <a href="#" class="btn btn-primary">Otwórz konto firmowe</a>
              @endif
            </div>
          </div>


        </div>
      </div>

      {{-- 
@endforeach
--}}
    </div>
  </div>
</div>
</div>
@endsection