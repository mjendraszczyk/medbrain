<div class="row">
    @foreach($ogloszenie as $item)
    <div class="oferta_detail">
        <div class="gray header_oferta_detail">
            <div class="col s12 m3 l3 white pading-20-0 box-radius-profil">
                  @if($item->logo_upload != '')
                    <img src="{{asset('img/logo')}}/{{$item->logo_upload}}" style="max-width: 100%;" />
                    @else
                    <img src="{{asset('img/logo-header.png')}}" style="max-width: 100%;" />
                    @endif

            </div>
            <div class="col s12 m9 l9 gray">
                <div class="row">
                    <div class="col s12 m8 l8  white-text">
                        <h4>{{$item->nazwa}}</h4>
                    </div>
                    <div class="col s12 m4 l4">
                        <a href="#" class="btn btn-default border-white">Aplikuj</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="white oferta_detail">
        <div class="col s12 m3 l3">
            <div class="main_detail">
                <h6 class="uppercase rem085 bold">
                    {{App\Http\Controllers\Controller::getSpecjalizacje($item->id_specjalizacje)->nazwa}}
                    {{-- @foreach(App\Http\Controllers\Controller::getSpecjalizacje($item->id_specjalizacje)
                    as $specjalizacja)
                    {{$specjalizacja->nazwa}}
                    @endforeach --}}

                </h6>
                <h4 class="gray-text rem1-25">
                    {{-- @foreach(App\Http\Controllers\Controller::getWynagrodzenie($item->wynagrodzenie)
                                    as $wynagrodzenie)
                                    {!!
                                    App\Http\Controllers\Controller::parseMoneyFormat($wynagrodzenie->nazwa) !!}
                                    @endforeach --}}
                    {!!
                    App\Http\Controllers\Controller::parseMoneyFormat($item->wynagrodzenie) !!}
                </h4>
            </div>
            <div class="address_detail">
                <h6 class="gray-text bold">
                       <img src="https://image.flaticon.com/icons/png/512/83/83909.png"
                    style="height: 24px;display: flex;">
                    {{App\Http\Controllers\Controller::getMiasto($item->id_miasta)}}
                </h6>
         
                <p>{{$item->nazwa}}</p>
                <p>{{$item->ulica}}</p>
                <h6 class="uppercase rem085">
                    <img src="https://image.flaticon.com/icons/png/512/149/149452.png"
                        style="height: 24px;display: block;">
                    Imię i Nazwisko</h6>
                {{$item->imie_nazwisko}}
                <h6 class="uppercase rem085">
                    <img src="https://image.flaticon.com/icons/png/512/1034/1034153.png"
                        style="height: 24px;display: block;">
                    Informacje kontaktowe</h6>
                <p>{{$item->podmiot_email}}</p>
                <div class="clearfix"></div>
                <p>{{$item->podmiot_telefon}}</p>
            </div>
        </div>
        <div class="col s12 m9 l9">
            <div class="row">
                <div class="col s12 m6 l6 xl3">
                    <h6 class="uppercase rem085">Wymiar pracy</h6>
                    <h5 class="bold gray-text">
                        @foreach(App\Http\Controllers\Controller::getWymiarPracy($item->id_wymiar_pracy)
                        as $wymiar)
                        {{$wymiar->nazwa}}
                        @endforeach
                    </h5>
                </div>
                <div class="col s12 m6 l6 xl3">
                    <h6 class="uppercase rem085">Doswiadczenie</h6>
                    <h5 class="bold gray-text">
                        {{$item->doswiadczenie_od}} - {{$item->doswiadczenie_do}} lat
                        {{-- @foreach(App\Http\Controllers\Controller::getDoswiadczenie($item->id_doswiadczenie)
                        as $doswiadczenie)
                        {{$doswiadczenie->nazwa}}
                        @endforeach --}}
                    </h5>
                </div>
                <div class="col s12 m6 l6 xl3">
                    <h6 class="uppercase rem085">Miasto</h6>
                    <h5 class="bold gray-text">
                        {{App\Http\Controllers\Controller::getMiasto($item->id_miasta)}}
                        {{-- @foreach(App\Http\Controllers\Controller::getMiasto($item->id_miasta)
                        as $miasto)
                        {{$miasto->nazwa}}
                        @endforeach --}}

                    </h5>
                </div>
                <div class="col s12 m6 l6 xl3">
                    <h6 class="uppercase rem085">Poziom stanowiska</h6>
                    <h5 class="bold gray-text">
                        {{(App\Http\Controllers\Controller::getPoziom($item->id_poziom)->nazwa)}}
                    </h5>
                </div>
            </div>
            <div class="row" style="display: block;">
                <h4 class="bold gray-text rem1-5 title-section">{{$item->nazwa}}</h4>
                {!!$item->tresc!!}

            </div>
        </div>
    </div>
    @endforeach
</div>