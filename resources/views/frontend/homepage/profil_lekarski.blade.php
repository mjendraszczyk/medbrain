<div class="row">
    {{-- @foreach($profil as $p) --}}
    <div class="oferta_detail">
        <div class="gray header_oferta_detail">
            <div class="col s12 m3 l3 pading-20-0 box-radius-profil no-padding">
                <div class="avatar">
                    @if($profil->avatar_upload != '')
                    <img src="{{asset('img/avatar')}}/{{$profil->avatar_upload}}">
                    @else
                    <img src="{{asset('img/icons/avatar.png')}}" />
                    @endif

                </div>
            </div>
            <div class="col s12 m9 l9 gray">
                <div class="row">
                    <div class="col s12 m8 l8  white-text">
                        <h4>{{$profil->name}}</h4>
                    </div>
                    <div class="col s12 m4 l4">
                        <a href="{{route('ogloszenia_specjalizacja_index',['id_specjalizacje'=>$profil->id_specjalizacje])}}"
                            class="btn btn-default border-white">Szukam pracy</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="white profil_detail">
        <div class="row">
            <div class="col s12 m3 l3">
                <img src="{{asset('img/icons/profil_specialization.png')}}" />
            </div>
            <div class="col s12 m9 l9">
                <div class="row">
                    <h6 class="uppercase rem085">Specjalizacja</h6>
                    <div class="clearfix"></div>
                    <h4 class="bold gray-text rem095">
                        @if($profil->id_specjalizacje == null) 
                            Niezdefiniowano
                        @else
                            {{App\Http\Controllers\Controller::getSpecjalizacje($profil->id_specjalizacje)->nazwa}}
                        @endif
                    </h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m3 l3">
                <img src="{{asset('img/icons/profil_experience.png')}}" />
            </div>
            <div class="col s12 m9 l9">
                <div class="row">
                    <h6 class="uppercase rem085">Doswiadczenie</h6>
                    <div class="clearfix"></div>
                    <h4 class="bold gray-text rem095">Szpital ABC Lorem ipsum w Szczecinie</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m3 l3">
                <img src="{{asset('img/icons/profil_education.png')}}" />
            </div>
            <div class="col s12 m9 l9">
                <div class="row">
                    <h6 class="uppercase rem085">Edukacja</h6>
                    <div class="clearfix"></div>
                    <h4 class="bold gray-text rem095">PUM w Szczecinie</h4>
                    <p>magister, Specjalizacja naurochirurgia</p>
                    <div class="clearfix"></div>
                    <p>2012-2017</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m3 l3">
                <img src="{{asset('img/icons/profil_about.png')}}" />
            </div>
            <div class="col s12 m9 l9">
                <div class="row">
                    <h6 class="uppercase rem085">O mnie</h6>
                    <div class="clearfix"></div>
                    {{$profil->o_mnie}}
                </div>
            </div>
        </div>
    </div>
    {{-- @endforeach --}}
</div>