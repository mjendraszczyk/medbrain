@extends('layouts.medbrain')

@section('content')
@if(Route::currentRouteName() == 'home')
<div class="section no-pad-bot" id="slider-banner">
    <div class="slogan-slider">
        <div class="container">
            <div class="slider-content">
                <h6 class="center gray-text">{{ config('app.name') }}</h6>
                <h1 class="header center gray-text">Pierwszy portal pracy</h1>
                <div class="row center">
                    <h3 class="green-text">Dla branzy medycznej</h3>
                </div>
            </div>
        </div>

    </div>
</div>
@endif
<div class="row flex">
    <div class="col s12 m6 bar-green-left typy-ogloszen dgrey" data-typy="lekarze">
        <h2>Szukaj lekarza <i class="material-icons">
                arrow_drop_down
            </i></h2>
    </div>
    <div class="col s12 m6 bar-gray-right typy-ogloszen green" data-typy="ogloszenia">
        <h2>Oferty pracy <i class="material-icons">
                arrow_drop_down
            </i></h2>
    </div>

</div>
<div id="app2">
    <div class="row">
        <div class="row panel white filtry">
            <div class="col s12 m12 l9 collapse">
                <h5>Specjalizacje</h5>
                <div class="ogloszenia_specjaliacje" data-typy="ogloszenia">
                <ul class="inline-list row">
                    @foreach($specjalizacje as $specjalizacja)
                    <li  class="col s12 m6 l4 xl2 pointer">
                        <a
                            href="{{route('ogloszenia_specjalizacja_index',['id_specjalizacje'=>$specjalizacja->id_specjalizacje])}}">
                            <span class="icon-specjalizacja"
                                style="background:#{{$specjalizacja->kolor}};">{{$specjalizacja->symbol}}</span>
                            {{$specjalizacja->nazwa}}
                        </a>
                    </li>
                    @endforeach
                </ul>
                </div>
                <div class="lekarze_specjalizacje hidden"  data-typy="lekarze">
                <ul class="inline-list row">
                    @foreach($specjalizacje as $specjalizacja)
                    <li class="col s12 m6 l4 xl2 pointer">
                        <a
                            href="{{route('lekarze_specjalizacja_index',['id_specjalizacje'=>$specjalizacja->id_specjalizacje])}}">
                            <span class="icon-specjalizacja"
                                style="background:#{{$specjalizacja->kolor}};">{{$specjalizacja->symbol}}</span>
                            {{$specjalizacja->nazwa}}
                        </a>
                    </li>
                    @endforeach
                </ul>
                </div>
            </div>

            <div class="col s12 m12 l3 collapse">
                <h5>Województwa</h5>
                <div class="ogloszenia_wojewodztwa"  data-typy="ogloszenia">
                <ul class="inline-list">
                    <li class="gray white-text center radius-15 indent-0 pointer">
                        <a href="{{route('home')}}">Wszystkie</a>
                    </li>
                    @foreach($wojewodztwa as $wojewodztwo)
                    <li class="gray white-text center radius-15 indent-0 pointer"><a
                            href="{{route('ogloszenia_wojewodztwa_index',['id_wojewodztwa'=>$wojewodztwo->id_wojewodztwa])}}">{{$wojewodztwo->nazwa}}</a>
                    </li>
                    @endforeach
                </ul>
                </div>
                <div class="lekarze_wojewodztwa hidden"  data-typy="lekarze">
                <ul class="inline-list">
                    <li class="gray white-text center radius-15 indent-0 pointer">
                        <a href="{{route('home')}}">Wszystkie</a>
                    </li>
                    @foreach($wojewodztwa as $wojewodztwo)
                    <li class="gray white-text center radius-15 indent-0 pointer"><a
                            href="{{route('lekarze_wojewodztwa_index',['id_wojewodztwa'=>$wojewodztwo->id_wojewodztwa])}}">{{$wojewodztwo->nazwa}}</a>
                    </li>
                    @endforeach
                </ul>
                </div>
            </div>
                            <span class="btn btn-primary more green"><i class="material-icons" style="vertical-align: middle;">
                arrow_drop_down
            </i>więcej</span>
        </div>
    </div>

    <div class="section">
        <!--   Icon Section   -->
        <div class="row praca-section">
            @if((Route::currentRouteName() == 'ogloszenia_zobacz') || (Route::currentRouteName() == 'profil_zobacz'))
            <div class="col s12 m12 l6" style="padding:25px">
                @else
                <div class="col s12 m12 l6 no-padding">
                    @endif
                    {{-- {{print_r($ogloszenia)}} --}}
                    @if((Route::currentRouteName() == 'ogloszenia_zobacz'))
                    @include('frontend.homepage.ogloszenie')
                    @elseif((Route::currentRouteName() == 'profil_zobacz'))
                    @include('frontend.homepage.profil_lekarski')
                    @else
                    <div class="oferty-pracy">
                        <ul class="lista">
                            {{-- 
                            <li class="row alert alert-danger white" v-if="informacje != ''">@{{informacje}}</li> --}}
                            {{-- 
                            <li class="row pointer" v-for="(ogloszenie,index) of ogloszenia" :key="index">
                                <a :href="'/ogloszenie/'+ogloszenie.id_ogloszenia">
                                    {{-- v-bind:data-url="{{route('home')}}/ogloszenie/@{{ogloszenie.id}}"
                            <span class="icon-specjalizacja"
                                :style="{background: colorMarker[index]}">@{{ogloszenie.symbol}}</span>
                            {{-- v-bind:style="{ background: '@{{ogloszenie.kolor}}' }"
                            <div class="col s12 m12 l8 text-left">
                                {{-- oglosznie.id_specjalizacje 
                                {{-- <h6>@{{getSpecjalizacjaName(ogloszenie.id_specjalizacje)}}</h6>
                                {{-- <h6>@{{specjalizacja[index]}}</h6>
                                <h3>@{{ogloszenie.nazwa}}</h3>
                                <div class="oferta-pracy_adres">@{{ogloszenie.nazwa}},
                                    @{{ogloszenie.kod_pocztowy}},
                                    @{{ogloszenie.ulica}}
                                </div>
                            </div>
                            <div class="col s12 m12 l4">
                                <div class="praca-wynagrodzenie">
                                    @{{ogloszenie.wynagrodzenie| currency}}
                                </div>
                            </div>
                            </a>
                            </li> --}}
                            @if(count($ogloszenia) > 0)
                            @foreach ($ogloszenia as $ogloszenie)
                            <li class="row pointer">
                                <a href="{{route('ogloszenia_zobacz',['id'=>$ogloszenie->id_ogloszenia])}}">
                                    <span class="icon-specjalizacja"
                                        style="background-color:#{{$ogloszenie->kolor}};">{{$ogloszenie->symbol}}</span>
                                    <div class="col s12 m12 l8">
                                        <h6>
                                            {{App\Http\Controllers\Controller::getSpecjalizacje($ogloszenie->id_specjalizacje)->nazwa}}
                                            {{-- @foreach(App\Http\Controllers\Controller::getSpecjalizacje($ogloszenie->id_specjalizacje)
                                        as $specjalizacja)
                                        {{$specjalizacja->nazwa}}
                                            @endforeach --}}
                                        </h6>
                                        <h3>{{$ogloszenie->nazwa}}</h3>
                                        <div class="oferta-pracy_adres">{{$ogloszenie->nazwa}},
                                            {{$ogloszenie->kod_pocztowy}},
                                            {{$ogloszenie->ulica}}
                                        </div>
                                    </div>
                                    <div class="col s12 m12 l4">
                                        <div class="praca-wynagrodzenie">

                                            {!!
                                            App\Http\Controllers\Controller::parseMoneyFormat($ogloszenie->wynagrodzenie)
                                            !!}
                                        </div>
                                    </div>
                                </a>
                            </li>
                            @endforeach
                            @else
                            <li class="row pointer alert alert-danger">
                                Brak ogłoszeń
                            </li>
                            @endif
                        </ul>
                    </div>
                    @endif

                </div>
                @include('frontend.homepage.map')
            </div>
        </div>
    </div>

    @endsection