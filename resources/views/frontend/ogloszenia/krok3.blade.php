@extends('layouts.medbrain')

@section('content')
<div class="row container center ogloszenia_create padding-box">
  <h6 class="bold gray-text">Nie czekaj</h6>
  <h1 class="green-text bold center">dodaj ogłoszenie</h1>
  <h3 class="gray-text center rem1-25 m20" style="">w <span class="rem1-75">4</span> prostych krokach</h3>

  <div class="row">
    <ul class="steeps col s8 offset-s2">
      <li class="col s3 steep">
      </li>
      <li class="col s3  steep">

      </li>
      <li class="col s3 steep current">
        <span class="steep-legend">
          Krok 3
        </span>
      </li>
      <li class="col s3  steep">

      </li>
    </ul>
  </div>

  <div class="row white card padding-50 radius-5">
    <h2 class="gray-text bold rem1-75">Uzupełnij dane kontaktowe</h2>

    <form method="POST" id="app" @submit="checkForm" novalidate action="{{route('krok3Request')}}">
      @include('backend/_main/message')
      @csrf
      <div class="input-field col s8 offset-s2">
        <div class="row">
          <div class="col s12 m6 l6">
            <input required placeholder="Nazwa firmy" name="podmiot" @if(Session::get('form_podmiot') !='' )
              value="{{Session::get('form_podmiot')}}" @else value="{{old('podmiot')}}" @endif type="text"
              class="form-control-input validate">
          </div>
          <div class="col s12 m6 l6">
            <input required placeholder="NIP" name="nip" @if(Session::get('form_nip') !='' )
              value="{{Session::get('form_nip')}}" @else value="{{old('nip')}}" @endif type="text"
              class="form-control-input validate">
          </div>

          <div class="col s12 m6 l6">
            <input required placeholder="Imię i nazwisko" name="imie_nazwisko"
              value="{{Session::get('form_imie_nazwisko')}}" type="text" class="form-control-input validate">
            <input required placeholder="Adres e-mail" @if(Session::get('form_adres_email') !='' )
              value="{{Session::get('form_adres_email')}}" @else value="{{old('adres_email')}}" @endif
              name="adres_email" type="text" class="form-control-input validate">
            <input required placeholder="Telefon" @if(Session::get('form_telefon') !='' )
              value="{{Session::get('form_telefon')}}" @else value="{{old('telefon')}}" @endif name="telefon"
              type="text" class="form-control-input validate">
          </div>
          <div class="col s12 m6 l6">
            <input required placeholder="Ulica" name="ulica" @if(Session::get('form_ulica') !='' )
              value="{{Session::get('form_ulica')}}" @else value="{{old('ulica')}}" @endif type="text"
              class="form-control-input validate">
            <select required name="miasto">
              <option value="" disabled selected>Województwo</option>
              @foreach ($wojewodztwa as $wojewodztwo)
              <optgroup label="{{$wojewodztwo->nazwa}}">
                @foreach (App\Http\Controllers\Controller::getMiastaByWojewodztwo($wojewodztwo->id_wojewodztwa) as
                $miasto)
                <option value="{{$miasto->id_miasta}}" @if(($miasto->id_miasta == Session::get('form_miasto')) ||
                  ($miasto->id_miasta == old('miasto')))
                  selected="selected"
                  @endif>{{$miasto->nazwa}}</option>
                @endforeach
              </optgroup>
              @endforeach
            </select>
            <select name="rodzaj_placowki">
              <option value="" disabled selected>Rodzaj placówki</option>
              @foreach ($rodzaje_placowki as $placowka)
              <option value="{{$placowka->id_rodzaj_placowki}}" @if(($placowka->id_rodzaj_placowki ==
                Session::get('form_rodzaj_placowki')) || ($placowka->id_rodzaj_placowki == old('rodzaj_placowki')))
                selected="selected"
                @endif>{{$placowka->nazwa}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <input type="text" required name="lat" id="lat" @if(Session::get('form_lat') !='' )
          value="{{Session::get('form_lat')}}" @else value="{{old('lat')}}" @endif />
        <input type="text" required name="lon" id="lon" @if(Session::get('form_lon') !='' )
          value="{{Session::get('form_lon')}}" @else value="{{old('lon')}}" @endif />

        <a href="{{route('oglosznia_dodaj_krok2')}}"
          class="m-t50 btn waves-effect waves-light btn-large gray-text border-btn min-200 pointer">Powrót</a>
        <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green min-200">Dalej</button>
      </div>


    </form>
  </div>
</div>

@endsection