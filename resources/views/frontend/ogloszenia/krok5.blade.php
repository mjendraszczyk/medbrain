@extends('layouts.medbrain')

@section('content')
<div class="row center ogloszenia_confirmation padding-box krok_5">
    <div class="container col s12 m6 l6 offset-l3 offset-m3">
        <h1 class="bold white-text">Gratulacje!</h1>

        <h6 class="white-text bold m-t50 m-d30">Właśnie założyłeś swoje konto na Medbra.pl. Od teraz możesz
            publikować ogłoszenia oraz założyć swoj profil lekarski.
        </h6>
        <p class="white-text">
            Potwierdź swoje konto oraz hasło linkiem
            dostarczonym na podany adres e-mail
        </p>
        <a href="#" class="btn waves-effect waves-light btn-large border-white">Przejdz do profilu</a>
    </div>
</div>
@endsection