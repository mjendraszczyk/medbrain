@extends('layouts.medbrain')

@section('content')
<div class="row container center ogloszenia_create padding-box">
  <h6 class="bold gray-text">Nie czekaj</h6>
  <h1 class="green-text bold center">dodaj ogłoszenie</h1>
  <h3 class="gray-text center rem1-25 m20" style="">w <span class="rem1-75">4</span> prostych krokach</h3>

  <div class="row">
    <ul class="steeps col s8 offset-s2">
      <li class="col s3 steep">
      </li>
      <li class="col s3  steep">
      </li>
      <li class="col s3 steep">
      </li>
      <li class="col s3  steep current">
        <span class="steep-legend">
          Krok 4
        </span>
      </li>
    </ul>
  </div>

  <div class="row white card padding-50 radius-5">
    <h2 class="gray-text bold rem1-75">Podsumowanie</h2>


    <div class="input-field col s8 offset-s2">
      <div class="row">
        <div class="col s12 m6 l6">
          <div class="row">
            <div class="col s12 c6 l6 text-left">
              Szukam
            </div>
            <div class="col s12 c6 l6 text-left">
              <strong>{{$szukam}}</strong>
            </div>

          </div>
          <hr />
        </div>
        <div class="col s12 m6 l6">
          <div class="row">
            <div class="col s12 c6 l6 text-left">
              Specjalizacja
            </div>
            <div class="col s12 c6 l6">
              <strong>{{$specjalizacja}}</strong>
            </div>
          </div>
          <hr />
        </div>

      </div>

      <div class="row">
        <div class="col s12 m6 l6 text-left">
          <h6 class="uppercase full-width rem08 m-15">Wymagania</h6>
          <div class="row">
            <div class="col s12 c6 l6">
              doswiadczenie
            </div>
            <div class="col s12 c6 l6">
              <strong>{{$doswiadczenie}}</strong>
            </div>
            <div class="col s12 c6 l6">
              stanowisko
            </div>
            <div class="col s12 c6 l6">
              <strong>{{$poziom}}</strong>
            </div>
          </div>
          <h6 class="uppercase full-width rem08 m-15">Informacje o pracy</h6>
          <div class="row">
            <div class="col s12 c6 l6">
              wymiar pracy
            </div>
            <div class="col s12 c6 l6">
              <strong>{{$wymiar_pracy}}</strong>
            </div>
            <div class="col s12 c6 l6">
              rodzaj umowy
            </div>
            <div class="col s12 c6 l6">
              <strong>
                <strong>{{$typ_umowy}}</strong>
              </strong>
            </div>
            <div class="col s12 c6 l6">
              wynagrodzenie
            </div>
            <div class="col s12 c6 l6">
              <strong>
                {!!
                App\Http\Controllers\Controller::parseMoneyFormat($wynagrodzenie) !!}
              </strong>
            </div>
          </div>
          <h6 class="uppercase full-width rem08 m-15">Dane kontaktowe</h6>
          <div class="row">
            <div class="col s12 c6 l6">
              miasto
            </div>
            <div class="col s12 c6 l6">
              {{$miasto}}
            </div>
            <div class="col s12 c6 l6">
              rodzaj placówki
            </div>
            <div class="col s12 c6 l6">
              <strong>{{$rodzaj_placowki}}</strong>
            </div>
          </div>
        </div>


        <div class="col s12 m6 l6 text-left">
          <h6 class="uppercase full-width rem08 m-15">Dane kontaktowe</h6>
          <div class="row">
            <div class="col s12 c6 l6">
              imię i nazwisko
            </div>
            <div class="col s12 c6 l6">
              <strong>{{$imie_nazwisko}}</strong>
            </div>
            <div class="col s12 c6 l6">
              e-mail
            </div>
            <div class="col s12 c6 l6">
              <strong>{{$email}}</strong>
            </div>
          </div>

          <h6 class="uppercase full-width rem08 m-15">Tresc ogloszenia</h6>
          <div class="row">
            <div class="col s12 c12 l12">
              {{$tresc}}
              <a href="#" class="btn-default table text-gray bold">Czytaj więcej >></a>
            </div>

          </div>
        </div>




      </div>
      <a href="{{route('oglosznia_dodaj_krok3')}}"
        class="m-t50 btn waves-effect waves-light btn-large gray-text border-btn min-200 pointer">Powrót</a>
      <form method="POST" id="app" @submit="checkForm" novalidate action="{{route('krok4Request')}}"
        style="display: inline;" class="white-text pointer">
        @csrf
        <button type="submit" class="pointer white-text m-t50 btn waves-effect waves-light btn-large green pointer">
          Dodaj ogłoszenie
        </button>
      </form>


    </div>
  </div>
</div>


@endsection