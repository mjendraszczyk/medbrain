@extends('layouts.medbrain')

@section('content')
<div class="row container center ogloszenia_create padding-box">
  <h6 class="bold gray-text">Nie czekaj</h6>
  <h1 class="green-text bold center">dodaj ogłoszenie</h1>
  <h3 class="gray-text center rem1-25 m20" style="">w <span class="rem1-75">4</span> prostych krokach</h3>

  <div class="row">
    <ul class="steeps col s8 offset-s2">
      <li class="col s3 steep current">
        <span class="steep-legend">
          Krok 1
        </span>
      </li>
      <li class="col s3  steep">

      </li>
      <li class="col s3  steep">

      </li>
      <li class="col s3  steep">

      </li>
    </ul>
  </div>

  <div class="row white card ogloszenie-krok1 padding-50 radius-5">
    <h2 class="gray-text bold rem1-75">Wybierz specjalizację</h2>
    <h4 class="gray-text rem1-25">z listy ponizej</h4>



    <form method="POST" id="app" novalidate @submit="checkForm" action="{{route('krok1Request')}}">
      @include('backend/_main/message')
      @csrf
      <div class="input-field col s6 offset-s3">
        <select name="specjalizacja" required>
          {{-- v-model="values['specjalizacja']" --}}
          <option value="" disabled selected>np. dermatologia</option>
          @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
          <option value="{{$specjalizacja->id_specjalizacje}}" @if(($specjalizacja->id_specjalizacje ==
            Session::get('form_specjalizacja')) || ($specjalizacja->id_specjalizacje == old('specjalizacja')))
            selected="selected"
            @endif>{{$specjalizacja->nazwa}}</option>
          @endforeach
        </select>
        <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green min-200">Dalej</button>
      </div>


    </form>
  </div>
</div>

@endsection