@extends('layouts.medbrain')

@section('content')
<div class="row container center ogloszenia_create padding-box">
  <h6 class="bold gray-text">Nie czekaj</h6>
  <h1 class="green-text bold center">dodaj ogłoszenie</h1>
  <h3 class="gray-text center rem1-25 m20" style="">w <span class="rem1-75">4</span> prostych krokach</h3>

  <div class="row">
    <ul class="steeps col s8 offset-s2">
      <li class="col s3 steep">
      </li>
      <li class="col s3  steep current">
        <span class="steep-legend">
          Krok 2
        </span>
      </li>
      <li class="col s3  steep">

      </li>
      <li class="col s3  steep">

      </li>
    </ul>
  </div>

  <div class="row white card padding-50 radius-5">
    <h2 class="gray-text bold rem1-75">Uzupełnij tresc ogłoszenia</h2>

    <form method="POST" id="app" @submit="checkForm" action="{{route('krok2Request')}}" novalidate>
      @include('backend/_main/message')
      @csrf
      <div class="input-field col s8 offset-s2">
        <div class="row">
          <div class="col s12 m6 l6">
            <select name="szukam" required>
              <option value="" disabled selected>Szukam</option>
              @foreach(App\Http\Controllers\Controller::getTypOgloszenia(null) as $typ)
              <option value="{{$typ->id_typ_ogloszenia}}" @if(($typ->id_typ_ogloszenia == Session::get('form_szukam'))
                || ($typ->id_typ_ogloszenia == old('szukam')))
                selected="selected"
                @endif>{{$typ->nazwa}}</option>
              @endforeach
            </select>

            <div class="row">
              <div class="col s12 m6 l6">
                <input required placeholder="Staz od" @if(Session::get('form_doswiadczenie_od') !='' )
                  value="{{Session::get('form_doswiadczenie_od')}}" @else value="{{old('doswiadczenie_od')}}" @endif
                  name="doswiadczenie_od" type="number" min="0" class="form-control-input validate" />
              </div>
              <div class="col s12 m6 l6">
                <input required placeholder="Staz do" @if(Session::get('form_doswiadczenie_do') !='' )
                  value="{{Session::get('form_wynagrodzenie')}}" @else value="{{old('doswiadczenie_do')}}" @endif
                  name="doswiadczenie_do" type="number" min="0" class="form-control-input validate" />
              </div>
            </div>
            <select name="poziom" required>
              <option value="" disabled selected>Poziom</option>
              @foreach(App\Http\Controllers\Controller::getPoziom(null) as $poziom)
              <option value="{{$poziom->id_poziom}}" @if(($poziom->id_poziom == Session::get('form_poziom')) ||
                ($poziom->id_poziom == old('poziom')))
                selected="selected"
                @endif>{{$poziom->nazwa}}</option>
              @endforeach
            </select>
          </div>
          <div class="col s12 m6 l6">
            <select name="typ_umowy" required>
              <option value="" disabled selected>Rodzaj umowy</option>
              @foreach(App\Http\Controllers\Controller::getRodzajUmowy(null) as $rodzaj)
              <option value="{{$rodzaj->id_rodzaj_umowy}}" @if(($rodzaj->id_rodzaj_umowy ==
                Session::get('form_typ_umowy')) || ($rodzaj->id_rodzaj_umowy == old('typ_umowy')))
                selected="selected"
                @endif>{{$rodzaj->nazwa}}</option>
              @endforeach
            </select>

            <select name="wymiar_pracy" required>
              <option value="" disabled selected>Wymiar pracy</option>
              @foreach(App\Http\Controllers\Controller::getWymiarPracy(null) as $wymiar)
              <option value="{{$wymiar->id_wymiar_pracy}}" @if(($wymiar->id_wymiar_pracy ==
                Session::get('form_wymiar_pracy')) || ($wymiar->id_wymiar_pracy == old('wymiar_pracy')))
                selected="selected"
                @endif>{{$wymiar->nazwa}}</option>
              @endforeach
            </select>
            <input required placeholder="Wynagrodzenie" @if(Session::get('form_wynagrodzenie') !='' )
              value="{{Session::get('form_wynagrodzenie')}}" @else value="{{old('wynagrodzenie')}}" @endif
              name="wynagrodzenie" type="number" min="0" class="form-control-input validate" />
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            @if(Session::get('form_tresc') != '')
            <textarea name="tresc" required placeholder="Wpisz tresc ogloszenia"
              class="form-control-area materialize-textarea validate">{{Session::get('form_tresc')}}</textarea>
            @else
            <textarea name="tresc" required placeholder="Wpisz tresc ogloszenia"
              class="form-control-area materialize-textarea validate">{{old('tresc')}}</textarea>
            @endif
          </div>
        </div>
        <a href="{{route('oglosznia_dodaj_krok1')}}"
          class="m-t50 btn waves-effect waves-light btn-large gray-text border-btn min-200 pointer">Powrót</a>
        <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green min-200">Dalej</button>
      </div>


    </form>
  </div>
</div>

@endsection