<footer class="page-footer">
  <div class="container">
    <div class="row">
      <div class="col l4 s12">
        <a href="{{route('home')}}"><img src="{{asset('img/logo-header.png')}}" /></a>
      </div>
      <div class="col l4 s12">
        <h5 class="">{{(App\Http\Controllers\Controller::getUstawienia()['firma'])}} </h5>
        <div class="footer-address">
          {{(App\Http\Controllers\Controller::getUstawienia()['adres'])}} <br />
        </div>
        <div class="footer-contact">
          tel. {{(App\Http\Controllers\Controller::getUstawienia()['telefon'])}} <br />
          email:
          {{(App\Http\Controllers\Controller::getUstawienia()['email'])}}
        </div>
      </div>
      <div class="col l4 s12">
        <ul class="footer-list-item">
          @include('frontend/_main/menu')
        </ul>
        <div class="copy">
          &copy; {{date('Y')}} {{(App\Http\Controllers\Controller::getUstawienia()['nazwa'])}} Wszelkie Prawa Zastrzezone
        </div>
      </div>
    </div>
  </div>

</footer>


<!--  Scripts-->

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{asset('js/materialize.js')}}"></script>
<script src="{{asset('js/init.js')}}"></script>

<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
  });

  $(document).ready(function(){
    //$('select').formSelect();
    $('select').select2({width: "100%"});
    M.updateTextFields();
  });


  
   document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
  });
  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
</script>


<script type="text/javascript">
  Vue.filter('currency', function (value) {
    return '' + parseFloat(value).toFixed(2)+' zł';
});

</script>
<!-- Include the Quill library -->
<script type="text/javascript" src="{{asset('js/quill.js')}}"></script>

<!-- Initialize Quill editor -->
<script>
  function getLanLon() {
  console.log("Test");
  console.log("BLABLA");
  var address = $("input[name=ulica]").val()+" "+$("select[name=miasto] :selected").text()+" "+$("select[name=id_miasta] :selected").text();
$.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q='+address, function(data){
       console.log(data);
       M.updateTextFields();
       
       if(data.length == 0) {
         var address = $("select[name=miasto] :selected").text()+" "+$("select[name=id_miasta] :selected").text();
         $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q='+address, function(data){
            $("#lat").val(data[0].lat);
            $("#lon").val(data[0].lon);
         });
       } else {
      $("#lat").val(data[0].lat);
      $("#lon").val(data[0].lon);
       }
});
}
</script>

@if((Route::currentRouteName() == 'home') || (Route::currentRouteName() == 'ogloszenia_zobacz') ||
(Route::currentRouteName() == 'profil_zobacz') || (Route::currentRouteName() == 'ogloszenia_specjalizacja_index') ||
(Route::currentRouteName() == 'ogloszenia_wojewodztwa_index'))
<script>
  function getMap() {
    @if(Route::currentRouteName() == 'ogloszenia_zobacz')
      @foreach($ogloszenie as $key => $o)
        @if($key == 0)
        var mymap = L.map('map').setView([{{$o->lat}},{{$o->lon}}], 16);
        @endif
      @endforeach
      @elseif(Route::currentRouteName() == 'ogloszenia_wojewodztwa_index')
        @foreach($ogloszenia as $key => $o)
          @if($key == 0)
            var mymap = L.map('map').setView([{{$o->lat}},{{$o->lon}}], 9);
          @endif
        @endforeach
        @if(count($ogloszenia) == 0) 
            var mymap = L.map('map').setView([52.3009024,19.7999369], 6);
            @endif
    @else
      var mymap = L.map('map').setView([52.3009024,19.7999369], 6);
    @endif
  var gl = L.mapboxGL({
  accessToken: 'pk.eyJ1IjoibWplbmRyYXN6Y3p5ayIsImEiOiJjazMzYjF5OXAwb2YzM2JwOW1xcHRmenAwIn0.xJ_cIRov3PlsgkxVKLryBg',
  attribution: '',
  maxZoom: 18,
  id: 'mapbox.streets',
  style: '{{asset("/js/style-map.json")}}'
  }).addTo(mymap);
  
  @foreach($ogloszenia as $o)
  var icon = L.divIcon({
  iconSize:null,
  html:'<div class="btn-floating pulse icon-specjalizacja" style="background:#{{$o->kolor}};">{{$o->symbol}} @if(count(App\Http\Controllers\Controller::getOgloszeniaDependsPodmiot($o->id_podmiot)) > 0) <sup class="counterMap">{{count(App\Http\Controllers\Controller::getOgloszeniaDependsPodmiot($o->id_podmiot))}}</sup> @endif</div>'});
  //<sup class="counterMap">{{rand(1,99)}}</sup>
  var marker = L.marker([{{$o->lat}}, {{$o->lon}}],{icon: icon}).addTo(mymap);
  marker.bindPopup("@if($o->logo_upload != '') <img src=\'{{asset('img/logo')}}/{{$o->logo_upload}}\' class='block m-auto'/> @else <img src='{{asset('img/logo-header.png')}}' class='block m-auto' /> @endif @if(count(App\Http\Controllers\Controller::getOgloszeniaDependsPodmiot($o->id_podmiot)) > 0) <ul class='oferty-pracy lista'> @foreach (App\Http\Controllers\Controller::getOgloszeniaDependsPodmiot($o->id_podmiot) as $o) <li class='row pointer'><a href=\"{{route('ogloszenia_zobacz',['id'=>$o->id_ogloszenia])}}\"><span class='icon-specjalizacja' style='background-color:#{{$o->kolor}};'>{{$o->symbol}}</span><div class='col s12 m12 l12'><h6>{{App\Http\Controllers\Controller::getSpecjalizacje($o->id_specjalizacje)->nazwa}}</h6><h3>{{App\Http\Controllers\Controller::getPoziom($o->id_poziom)->nazwa}} {{App\Http\Controllers\Controller::getSpecjalizacje($o->id_specjalizacje)->nazwa}}</h3><div class='oferta-pracy_adres'>{{$o->nazwa}},{{$o->kod_pocztowy}},{{$o->ulica}}</div></div><div class='col s12 m12 l12'><div class='praca-wynagrodzenie'>{!!App\Http\Controllers\Controller::parseMoneyFormat($o->wynagrodzenie)!!}</div></div></a></li>@endforeach </ul> @endif");
  @if(Route::currentRouteName() == 'ogloszenia_zobacz')
  @foreach($ogloszenie as $item)
  @if($item->id_ogloszenia == $o->id_ogloszenia)
  marker.openPopup();
  @endif
  @endforeach
  @endif
  @endforeach
  
  }
  $(document).ready(function(){
    getMap();
});
</script>
@endif
<script>
  $("input, select").change(function() { 
    
    getLanLon();
    }).ready(function() {
    getLanLon();
  });
   
  var i = 0;
  $(".more").click(function(){
    if (i%2 == 0) {
      $(this).parent().children('.collapse').css('height','auto'); 
      $(this).parent().children('.collapse').css('overflow','visible');
      $(this).html("<i class='material-icons'>arrow_drop_up</i>Mniej");
    } else {
      $(this).parent().children('.collapse').css('height','200px'); 
      $(this).parent().children('.collapse').css('overflow','hidden');
      $(this).html("<i class='material-icons'>arrow_drop_down</i>Więcej");
    }
    i++;
    });
$(".typy-ogloszen").click(function() {
  var getTyp = $(this).attr('data-typy');
  $('.typy-ogloszen').removeClass('green');
  $('.typy-ogloszen').addClass('dgrey');

    $(this).removeClass('dgray');
    $(this).addClass('green');

  if(getTyp == 'lekarze') {
    // alert("lekarze");
    $('.filtry [data-typy="ogloszenia"]').addClass('hidden');
    $('.filtry [data-typy="lekarze"]').removeClass('hidden');
  } else {
    // alert("ogloszenia");
    $('.filtry [data-typy="ogloszenia"]').removeClass('hidden');
    $('.filtry [data-typy="lekarze"]').addClass('hidden');
  }
})

</script>

{{--@if((Route::currentRouteName() == 'home') || (Route::currentRouteName() == 'ogloszenia_zobacz') ||
(Route::currentRouteName() == 'profil_zobacz'))
<script>
  var app2 = new Vue({
  el: '#app2',
  created() {
  // this.getOgloszenia(0, 0);
  },
  mounted() {
    this.getMap();
  },
  data: {
    ogloszenia: [],
    informacje: '',
    activeClass: false,
    errorClass: true,
    specjalizacja: [],
    active: false,
    activeSpecjalizacja:false,
    colorMarker: []
  },
  methods: {
      getOgloszenia(id_wojewodztwo, id_specjalizacja) {
      console.log("dsfdsf"+id_wojewodztwo+' '+id_specjalizacja);
      //api/ogloszenia/w/0/s/0
      this.colorMarker = [];
      axios.get('{{route("home")}}/api/ogloszenia/w/'+id_wojewodztwo+'/s/'+id_specjalizacja+'').then(response => {
//https://jsonplaceholder.typicode.com/posts
if(response.data.length == 0) {
this.informacje = "Brak ogłoszeń spełniających warunek";
this.ogloszenia = [];
this.specjalizacja = [];
} else {
this.informacje = "";
this.ogloszenia = response.data;

//this.getMap();
//map.panTo([32.3009024,19.7999369], 8);
// var mymap;
// mymap.panTo([new L.LatLng(32.3009024,19.7999369)]);
// mymap.setZoom(11);

console.log("dfsd");
//console.log("TEST"+response.data.id_ogloszenia);
for(var i=0;i<response.data.length;i++) { this.getSpecjalizacjaName(response.data[i].id_specjalizacje);
  this.colorMarker.push("#"+response.data[i].kolor); } console.log(response.data) } }); },
  getSpecjalizacjaName(id_specjalizacje) { //api/ogloszenia/specjalizacja/3
  axios.get('{{route("home")}}/api/ogloszenia/specjalizacja/'+id_specjalizacje).then(response=> {
  console.log(response.data); // this.specjalizacja=response.data; var id_specjalizacje=response.data.id_specjalizacje;
  this.id_specjalizacje.push(response.data); //return this.specjalizacja; }); }, getMap() { console.log("111"); var
  mymap=L.map('map').setView([52.3009024,19.7999369], 6); var gl=L.mapboxGL({
  accessToken: 'pk.eyJ1IjoibWplbmRyYXN6Y3p5ayIsImEiOiJjazMzYjF5OXAwb2YzM2JwOW1xcHRmenAwIn0.xJ_cIRov3PlsgkxVKLryBg' ,
  attribution: '' , maxZoom: 18, id: 'mapbox.streets' , style: '{{asset("/js/style-map.json")}}' }).addTo(mymap);
  @foreach($ogloszenia as $o) var icon=L.divIcon({ iconSize:null, html:'<div
  class="btn-floating pulse icon-specjalizacja" style="background:#{{$o->kolor}};">
  {{$o->symbol}}</div>'
  });
  //<sup class="counterMap">{{rand(1,99)}}</sup>
  var marker = L.marker([{{$o->lat}}, {{$o->lon}}],{icon: icon}).addTo(mymap);
  marker.bindPopup("<img src='{{asset('img/logo-header.png')}}'
    class='block m-auto' /><br /><b>{{$o->nazwa}}</b><br />{{$o->ulica}}<br />tel:{{$o->podmiot_telefon}}<br />email:{{$o->podmiot_email}}");
  @endforeach
  }
  }
  });
  </script>
  @endif--}}
  </body>

  </html>