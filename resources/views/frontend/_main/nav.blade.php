
       <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
       <ul class="right hide-on-med-and-down">
        <li><a href="{{route('oglosznia_dodaj_krok1')}}">
        <i class="material-icons">
                    message
                </i>
        Dodaj ogloszenie</a></li>
        @if (Auth::guest())
        <li><a href="{{route('login')}}">
        <i class="material-icons">
                    people_outline
                </i>  
        Zaloguj się</a></li>
        @else
        <li><a href="{{route('profil_dane')}}">
        <i class="material-icons">
                    people_outline
                </i>  
        Profil</a></li>
        @if(Auth::user()->id_rola == '3') 
        <li><a href="{{route('backend_index')}}">
       <i class="material-icons">
        dashboard
        </i>
        Admin</a></li>
        @endif
        @endif
      </ul>