@extends('layouts.medbrain_backend')

@section('content')
<div class="container">
    <div class="card">
        @include('backend.poziom.header')
        <div class="card-body">
            <form method="POST" action="{{route('backend_poziom_store')}}">
                @include('backend.poziom.form')
            </form>
        </div>
    </div>
</div>
@endsection