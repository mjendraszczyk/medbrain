@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Rodzaj placówki
            <a href="{{route('backend_rodzaj_placowki_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{route('backend_rodzaj_placowki_store')}}">
                @include('backend.rodzaj_placowki.form')
            </form>
        </div>
    </div>
</div>
@endsection