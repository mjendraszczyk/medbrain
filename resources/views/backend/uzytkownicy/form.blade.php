@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa firmy</label>
    @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )

    @endif
    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        value="{{$uzytkownicy->name}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>Email</label>
    <input required placeholder="E-mail" name="email" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        value="{{$uzytkownicy->email}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>Hasło</label>
    <input placeholder="Hasło" name="password" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' ) value=""
        @else value="" @endif type="password" class="form-control-input validate">
    <label>Powtórz hasło</label>
    <input placeholder="Powtórz hasło" name="password_repeat" @if(Route::currentRouteName()=='backend_uzytkownicy_edit'
        ) value="" @else value="" @endif type="password" class="form-control-input validate">

    <label>Rola</label>
    <select name="id_rola">
        <option value="" disabled selected>rola</option>
        @foreach(App\Http\Controllers\Controller::getRole(null) as $rola)
        <option value="{{$rola->id_rola}}" @if((Route::currentRouteName()=='backend_uzytkownicy_edit' ) &&
            ($uzytkownicy->id_rola ==
            $rola->id_rola))
            selected="selected"
            @else
            @endif>{{$rola->nazwa}}</option>
        @endforeach
    </select>
    <div class="clearfix"></div>
    <label>Status</label>
    <select name="stan">
        @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        <option value="1" @if($uzytkownicy->stan == '1') selected="selected" @endif >Włączone</option>
        <option value="0" @if($uzytkownicy->stan != '1') selected="selected" @endif>Wyłączone</option>
        @else
        <option value="1">Włączone</option>
        <option value="0" selected="selected">Wyłączone</option>
        @endif
    </select>

</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>