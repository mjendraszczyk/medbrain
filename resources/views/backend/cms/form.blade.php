@csrf

<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Tutuł</label>
    <input required placeholder="Tytuł" name="tytul" @if(Route::currentRouteName()=='backend_cms_edit' )
        value="{{$cms->tytul}}" @else value="" @endif type="text" class="form-control-input validate">
    <label>Tresc</label>
    <textarea placeholder="Tresc" name="tresc"
        class="form-control-area materialize-textarea validate">@if(Route::currentRouteName()=='backend_cms_edit' ){{$cms->tresc}} @else @endif </textarea>
    <label>Stan</label>

    <select name="stan">
        @if(Route::currentRouteName()=='backend_cms_edit' )
        <option value="1" @if($cms->stan == '1') selected="selected" @endif >Włączone</option>
        <option value="0" @if($cms->stan != '1') selected="selected" @endif>Wyłączone</option>
        @else
        <option value="1">Włączone</option>
        <option value="0" selected="selected">Wyłączone</option>
        @endif
    </select>
</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>