    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (!empty(Session::get('status')))
    <div class="alert alert-success">
        <ul>
            <li>{{Session::get('status')}}</li>
        </ul>
    </div>
    @endif
    @if (!empty(Session::get('status_fail')))
    <div class="alert alert-danger">
        <ul>
            <li>{{Session::get('status_fail')}}</li>
        </ul>
    </div>
    @endif
    