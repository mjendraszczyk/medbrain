@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Ogłoszenia
            <a href="{{route('backend_ogloszenia_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{route('backend_ogloszenia_store')}}">
                @include('backend.ogloszenia.form')
            </form>
        </div>
    </div>
</div>
@endsection