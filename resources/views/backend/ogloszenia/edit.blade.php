@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Ogłoszenia
            <a href="{{route('backend_podmiot_create')}}" class="btn waves-effect waves-light btn-third" style="background: #2d3040;margin:0;">
                <i class="material-icons">business_center</i>
                Dodaj firmę</a>

            <a target="_blank" href="{{route('ogloszenia_zobacz',['id_ogloszenia'=>$ogloszenia->id_ogloszenia])}}" class="btn waves-effect waves-light btn-third">
                <i class="material-icons">remove_red_eye</i>
                Podgląd</a>
            <a href="{{route('backend_ogloszenia_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{route('backend_ogloszenia_update', ['id' => $ogloszenia->id_ogloszenia])}}">
                @method('PUT')
                @include('backend.ogloszenia.form')
            </form>
        </div>
    </div>
</div>
@endsection