@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Ustawienia
            <a href="{{route('backend_ustawienia_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{route('backend_ustawienia_update', ['id' => $ustawienia->id_ustawienia])}}">
                @method('PUT')
                @include('backend.ustawienia.form')
            </form>
        </div>
    </div>
</div>
@endsection