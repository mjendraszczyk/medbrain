@csrf
<div class="col s12 m12 l12">
            @include('backend/_main/message')
    <label>Instancja</label> 


    <input readonly placeholder="Instancja" name="instancja" @if(Route::currentRouteName()=='backend_ustawienia_edit' )
        value="{{$ustawienia->instancja}}" @else value="" @endif type="text" class="form-control-input validate">
 
    <label>Wartosc</label> 
    
    <input required placeholder="Wartosc" name="wartosc" @if(Route::currentRouteName()=='backend_ustawienia_edit' )
        value="{{$ustawienia->wartosc}}" @else value="" @endif type="text" class="form-control-input validate">
 
    </div>
</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>