@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa miasta</label>

    <input required placeholder="Nazwa" name="nazwa" @if(Route::currentRouteName()=='backend_miasta_edit' )
        value="{{$miasta->nazwa}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>Województwo</label>

    <select required name="id_wojewodztwa">
        <option value="" disabled selected>Województwo</option>

        @foreach (App\Http\Controllers\Controller::getWojewodztwo(null) as
        $wojewodztwo)
        <option value="{{$wojewodztwo->id_wojewodztwa}}" @if(Route::currentRouteName()=='backend_miasta_edit' ) @if(
            $wojewodztwo->id_wojewodztwa ==
            $miasta->id_wojewodztwa)
            selected="selected"
            @endif
            @endif
            >{{$wojewodztwo->nazwa}}</option>
        @endforeach
    </select>

</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>