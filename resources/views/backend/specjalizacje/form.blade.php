@csrf
<div class="col s12 m12 l12">
    
@include('backend/_main/message')
    <label>Specjalizacje</label> 
    
    <input required placeholder="Nazwa" name="nazwa" @if(Route::currentRouteName()=='backend_specjalizacje_edit' )
        value="{{$specjalizacje->nazwa}}" @else value="" @endif type="text" class="form-control-input validate">


            <!-- <select required name="nazwa">

        <option value="" disabled selected>Specjalizacja</option>

        @foreach (App\Http\Controllers\Controller::getSpecjalizacje(null) as
        $specjalizacja)
        <option value="{{$specjalizacja->id_specjalizacje}}" @if(Route::currentRouteName()=='backend_specjalizacje_edit' ) @if( $specjalizacja->
            id_specjalizacje ==
            $specjalizacje->id_specjalizacje)
            selected="selected"
            @endif
            @endif
            >{{$specjalizacja->nazwa}}</option>
        @endforeach
    </select> -->

    <label>Kolor</label>
<input required placeholder="Kolor" style="width:100%;" name="kolor" @if(Route::currentRouteName()=='backend_specjalizacje_edit' )
        value="#{{$specjalizacje->kolor}}" @else value="" @endif type="color" class="form-control-input validate">

    <label>Symbol</label>
<input required placeholder="Symbol" name="symbol" @if(Route::currentRouteName()=='backend_specjalizacje_edit' )
        value="{{$specjalizacje->symbol}}" @else value="" @endif type="text" class="form-control-input validate">
    </div>
</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>