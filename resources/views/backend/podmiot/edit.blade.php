@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Firma
            <a href="{{route('backend_podmiot_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{route('backend_podmiot_update', ['id' => $podmiot->id_podmiot])}}">
                @method('PUT')
                @include('backend.podmiot.form')
            </form>
        </div>
    </div>
</div>
@endsection