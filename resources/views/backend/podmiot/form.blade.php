@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa firmy</label>
    
    <input required placeholder="Nazwa firmy" name="nazwa" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->nazwa}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>NIP</label>
    <input required placeholder="NIP" name="nip" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->nip}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>Ulica</label>
    <input required placeholder="Ulica" name="ulica" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->ulica}}" @else value="" @endif type="text" class="form-control-input validate">

    <div class="row">
        <div class="col s12 m4 l4">
            <label>Kod pocztowy</label>
            <input required placeholder="Kod poczyowy" name="kod_pocztowy"
                @if(Route::currentRouteName()=='backend_podmiot_edit' ) value="{{$podmiot->kod_pocztowy}}" @else
                value="" @endif type="text" class="form-control-input validate">
        </div>
        <div class="col s12 m8 l8">
            <label>Miasto</label>
            <select name="id_miasta">
                <option value="" disabled selected>Miasto</option>
                @foreach ($wojewodztwa as $wojewodztwo)
                <optgroup label="{{$wojewodztwo->nazwa}}">
                    @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo($wojewodztwo->id_wojewodztwa)
                    as
                    $miasto)
                    <option value="{{$miasto->id_miasta}}" @if((Route::currentRouteName()=='backend_podmiot_edit' ) && ($podmiot->id_miasta == $miasto->id_miasta))
                        selected="selected" @else @endif>{{$miasto->nazwa}}</option>
                    @endforeach
                </optgroup>
                @endforeach
            </select>
        </div>
    </div>
    <label>Telefon</label>
    <input required placeholder="Telefon" name="podmiot_telefon" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->podmiot_telefon}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>E-mail</label>
    <input required placeholder="E-mail" name="podmiot_email" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->podmiot_email}}" @else value="" @endif type="text" class="form-control-input validate">

    <input required placeholder="Lat" id="lat" name="lat" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->lat}}" @else value="" @endif type="text" class="form-control-input validate">
    <input required placeholder="Lon" id="lon" name="lon" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->lon}}" @else value="" @endif type="text" class="form-control-input validate">

</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>